package middleware

import (
	"github.com/gofiber/fiber/v2"
	jwtware "github.com/gofiber/jwt/v3"
	"github.com/spf13/viper"
	"gitlab.com/rodrigoodhin/go-2fa-api/app/utils/jwt"
)

// IsRequired - Authentication middleware that returns a new jwt
func IsRequired() fiber.Handler {
	return jwtware.New(jwtware.Config{
		SigningKey:     []byte(viper.GetString("jwt.secret")),
		ErrorHandler:   jwt.Error,
		SuccessHandler: jwt.Success,
	})
}

// IsRequiredOTP - Authentication middleware for OTP that returns a new jwt
func IsRequiredOTP() fiber.Handler {
	return jwtware.New(jwtware.Config{
		SigningKey:     []byte(viper.GetString("jwt.otpSecret")),
		ErrorHandler:   jwt.Error,
		SuccessHandler: jwt.Success,
	})
}

// IsRequiredPassReset - Authentication middleware for Password Reset that returns a new jwt
func IsRequiredPassReset() fiber.Handler {
	return jwtware.New(jwtware.Config{
		SigningKey:     []byte(viper.GetString("jwt.passResetSecret")),
		ErrorHandler:   jwt.Error,
		SuccessHandler: jwt.Success,
	})
}
