package jwt

import (
	"bytes"
	"encoding/base64"
	"encoding/json"
	"image/png"
	"log"
	"math/rand"
	"time"

	"github.com/gofiber/fiber/v2"
	"github.com/golang-jwt/jwt/v4"
	"github.com/pquerna/otp"
	"github.com/pquerna/otp/totp"
	"github.com/spf13/viper"
	"gitlab.com/rodrigoodhin/go-2fa-api/app"
	"gitlab.com/rodrigoodhin/go-2fa-api/app/types"
	"gitlab.com/rodrigoodhin/go-2fa-api/internal/handlers"
)

// Error - Handler error for authentication middleware
func Error(c *fiber.Ctx, err error) error {
	if err.Error() == "Missing or malformed JWT" {
		return handlers.Response{
			Ctx:    c,
			Status: fiber.StatusBadRequest,
			Msg:    "Missing or malformed token",
			Data:   fiber.Map{"error": err.Error()},
		}.Send()
	}
	return handlers.Response{
		Ctx:    c,
		Status: fiber.StatusUnauthorized,
		Msg:    "Invalid or expired token",
		Data:   fiber.Map{"error": err.Error()},
	}.Send()
}

// Success - Handler success for authentication middleware
func Success(c *fiber.Ctx) error {
	// Get user token information saved at data store
	tokenCache, err := GetTokenCache(c)
	if err != nil {
		return handlers.Response{
			Ctx:    c,
			Status: fiber.StatusUnauthorized,
			Msg:    "Invalid token",
			Data:   fiber.Map{"error": err.Error()},
		}.Send()
	}

	// Compare token value with user id
	if tokenCache.UserID.String() != GetUserId(c) {
		return handlers.Response{
			Ctx:    c,
			Status: fiber.StatusUnauthorized,
			Msg:    "Invalid token",
			Data:   fiber.Map{"error": err.Error()},
		}.Send()
	}

	// Go to the next middleware
	return c.Next()
}

// GetReqTokenRaw - Get token from request
func GetReqTokenRaw(c *fiber.Ctx) string {
	return c.Locals("user").(*jwt.Token).Raw
}

// GetClaims - Get claims from request
func GetClaims(c *fiber.Ctx) (claims jwt.MapClaims) {
	usr := c.Locals("user").(*jwt.Token)
	claims = usr.Claims.(jwt.MapClaims)
	return
}

// GenerateToken - Generate a new token
func GenerateToken(user *types.User, secret string, si *types.SessionInfo) (t string, err error) {
	// Create token
	token := jwt.New(jwt.SigningMethodHS256)

	// Set session timeout
	sessionTimeout := GetSessionTimeout()

	// Prepare dates
	timeNow := time.Now()
	expireAt := timeNow.Add(sessionTimeout)

	// Set claims
	claims := token.Claims.(jwt.MapClaims)
	claims["id"] = user.ID
	claims["exp"] = expireAt.Unix()

	// Generate encoded token and send it as response.
	t, err = token.SignedString([]byte(secret))
	if err != nil {
		log.Fatalf("Failed to generate a new token: %v", err)
	}

	// Create token cache instance
	tokenCache := new(types.TokenCache)
	tokenCache.UserID = user.ID
	tokenCache.UserPermits = user.Permits
	tokenCache.CreatedAt = timeNow
	tokenCache.ExpireAt = expireAt
	tokenCache.Browser = si.Browser
	tokenCache.IP = si.IP
	tokenCache.System = si.System

	// Marshal token cache
	tc, err := json.Marshal(tokenCache)
	if err != nil {
		log.Fatalf("Failed to parse token cache to map: %v", err)
	}

	// Save token in data store
	err = app.TokenCache.Set(t, string(tc), sessionTimeout).Err()
	//err = d.TokenCache.HMSet(t, string(tc), sessionTimeout).Err()
	if err != nil {
		log.Fatalf("Failed to call Put: %v", err)
	}

	return
}

// GetSessionTimeout - Calculate session timeout
func GetSessionTimeout() time.Duration {
	return viper.GetDuration("jwt.sessionTimeout") * time.Minute
}

// GetTokenCache - Get user token information saved at data store
func GetTokenCache(c *fiber.Ctx) (tc *types.TokenCache, err error) {
	tokenCacheValue, err := app.TokenCache.Get(GetReqTokenRaw(c)).Result()

	if tokenCacheValue != "" {
		err = json.Unmarshal([]byte(tokenCacheValue), &tc)
	}

	return
}

// GenerateOTP - Generate a new OTP key for user
func GenerateOTP(email string) (key *otp.Key) {
	key, err := totp.Generate(totp.GenerateOpts{
		Issuer:      viper.GetString("url"),
		AccountName: email,
	})
	if err != nil {
		log.Fatalf("Failed to generate a new OTP key: %v", err)
	}

	return
}

// ValidateOTP - Validate the OTP key passcode for user
func ValidateOTP(key, passcode string) bool {
	otpKey, err := otp.NewKeyFromURL(key)
	if err != nil {
		log.Fatalf("Failed to validate the OTP key passcode: %v", err)
	}

	return totp.Validate(passcode, otpKey.Secret())
}

// GenerateRecoveryCodes - Generate OTP recovery codes
func GenerateRecoveryCodes() (recoveryCodes []string) {
	letterBytes := viper.GetString("otp.recoveryCodesLetterBytes")

	for k := 0; k < 16; k++ {

		a := make([]byte, 5)
		for i := range a {
			a[i] = letterBytes[rand.Intn(len(letterBytes))]
		}

		b := make([]byte, 5)
		for i := range b {
			b[i] = letterBytes[rand.Intn(len(letterBytes))]
		}

		recoveryCodes = append(recoveryCodes, string(a)+"-"+string(b))
	}

	return
}

// GenerateOTPQRCode - Convert TOTP key into a base64 image
func GenerateOTPQRCode(key *otp.Key) (imgBase64 string) {
	var buf bytes.Buffer

	img, err := key.Image(200, 200)
	if err != nil {
		log.Fatalf("Failed to generate OTP QR Code: %v", err)
	}

	err = png.Encode(&buf, img)
	if err != nil {
		log.Fatalf("Failed to encode OTP QR Code: %v", err)
	}

	imgBase64 = base64.StdEncoding.EncodeToString(buf.Bytes())

	return
}

// GetUserId - Ger User Id from claims
func GetUserId(c *fiber.Ctx) string {
	return GetClaims(c)["id"].(string)
}

// GetTokenUserId - Ger User Id from token
func GetTokenUserId(tokenString string) (token string, err error) {
	claims := jwt.MapClaims{}

	_, err = jwt.ParseWithClaims(tokenString, claims, func(token *jwt.Token) (interface{}, error) {
		return []byte(viper.GetString("jwt.passResetSecret")), nil
	})

	token = claims["id"].(string)

	return
}
