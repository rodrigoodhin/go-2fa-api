package utils

import (
	"gitlab.com/rodrigoodhin/go-2fa-api/app/types"
	"gitlab.com/rodrigoodhin/go-2fa-api/internal/database"
)

// CreateResponseUser - Returns a UserResponse object from a User object
func CreateResponseUser(user types.User) types.UserResponse {
	return types.UserResponse{
		ID:       user.ID,
		Username: user.Username,
		Email:    user.Email,
		Status:   user.Status,
		Permits:  user.Permits,
		UserType: user.UserType,
	}
}

// argon2$4$32768$4$32$rrzmVBUF3hCYMhcfcLkxmw==$KVQp4wjAYVP1qiy9OTmRlpMEs2duqxoWPhVLGnzkkhI=
// CreateResponseUsers - Returns a []UserResponse object from a User object
func CreateResponseUsers(users []types.User) (usersResponse []types.UserResponse) {
	for _, user := range users {
		usersResponse = append(usersResponse, CreateResponseUser(user))
	}

	return
}

// CreateUser - Returns a User object from a UserUpsert object
func CreateUser(user types.UserUpsert) types.User {
	return types.User{
		Model: database.Model{
			ID: user.ID,
		},
		Username: user.Username,
		Password: user.Password,
		Email:    user.Email,
		Status:   user.Status,
		UserType: user.UserType,
	}
}
