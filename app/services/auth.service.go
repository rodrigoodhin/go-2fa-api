package services

import (
	"encoding/json"
	"log"
	"strings"

	"gorm.io/datatypes"

	"github.com/gofiber/fiber/v2"
	"github.com/pquerna/otp"
	"github.com/pquerna/otp/totp"
	"github.com/spf13/viper"
	"gitlab.com/rodrigoodhin/go-2fa-api/app"
	"gitlab.com/rodrigoodhin/go-2fa-api/app/dal"
	"gitlab.com/rodrigoodhin/go-2fa-api/app/types"
	"gitlab.com/rodrigoodhin/go-2fa-api/app/utils/jwt"
	"gitlab.com/rodrigoodhin/go-2fa-api/internal/handlers"
)

// Login
// @Summary user login
// @Description validates login information, creates token and send it in response
// @Tags auth
// @Accept json
// @Produce json
// @Param message body types.LoginRequest true "Login Info"
// @Success 200 {object} handlers.ResponseData
// @Failure 400 {object} handlers.ResponseData
// @Failure 401 {object} handlers.ResponseData
// @Failure 406 {object} handlers.ResponseData
// @Failure 500 {object} handlers.ResponseData
// @Router /auth [post]
func Login(c *fiber.Ctx) (err error) {
	loginRequest := new(types.LoginRequest)
	err = c.BodyParser(loginRequest)
	if err != nil {
		return handlers.Response{
			Ctx:    c,
			Status: fiber.StatusBadRequest,
			Msg:    "Request body is invalid",
			Data:   fiber.Map{"error": err.Error()},
		}.Send()
	}

	err = loginRequest.Validate()
	if err != nil {
		return handlers.Response{
			Ctx:    c,
			Status: fiber.StatusNotAcceptable,
			Msg:    "Invalid login information",
			Data:   fiber.Map{"error": err.Error()},
		}.Send()
	}

	var user types.User

	// validate if user exits in db
	if user, err = dal.Login(loginRequest.Username, loginRequest.Password); err != nil {
		return handlers.Response{
			Ctx:    c,
			Status: fiber.StatusUnauthorized,
			Msg:    "Invalid username or password",
			Data:   fiber.Map{"error": err.Error()},
		}.Send()
	}

	var si types.SessionInfo
	si.Browser = loginRequest.Browser
	si.IP = loginRequest.IP
	si.System = loginRequest.System

	if user.OTPStatus {
		// Generate encoded otp token and send it as response.
		t, err := jwt.GenerateToken(&user, viper.GetString("jwt.otpSecret"), &si)
		if err != nil {
			return handlers.Response{
				Ctx:    c,
				Status: fiber.StatusInternalServerError,
				Msg:    "Error generating token",
				Data:   fiber.Map{"error": err.Error()},
			}.Send()
		}

		return handlers.Response{
			Ctx:    c,
			Status: fiber.StatusOK,
			Data:   fiber.Map{"otp": t, "user_type": user.UserType},
		}.Send()

	} else {
		// Generate encoded token and send it as response.
		t, err := jwt.GenerateToken(&user, viper.GetString("jwt.secret"), &si)
		if err != nil {
			return handlers.Response{
				Ctx:    c,
				Status: fiber.StatusInternalServerError,
				Msg:    "Error generating token",
				Data:   fiber.Map{"error": err.Error()},
			}.Send()
		}

		return handlers.Response{
			Ctx:    c,
			Status: fiber.StatusOK,
			Data:   fiber.Map{"token": t, "user_type": user.UserType},
		}.Send()
	}
}

// Profile
// @Summary user profile
// @Description retrieves user profile data
// @Tags auth
// @Produce json
// @Success 200 {object} handlers.ResponseData
// @Failure 400 {object} handlers.ResponseData
// @Failure 401 {object} handlers.ResponseData
// @Security ApiKeyAuth
// @param Authorization header string true "Authorization"
// @Router /auth [get]
func Profile(c *fiber.Ctx) (err error) {
	var sessions []types.ProfileResponse

	a := strings.Split(jwt.GetReqTokenRaw(c), ".")
	t := a[1][23:]

	s := app.TokenCache.Keys("*" + t + "*")
	r, err := s.Result()
	if err != nil {
		log.Fatalln(err)
	}

	for _, k := range r {
		session := types.ProfileResponse{}

		session.SessionKey = k

		v := app.TokenCache.Get(k).Val()

		if err := json.Unmarshal([]byte(v), &session.SessionValue); err != nil {
			log.Fatalln(err)
		}

		sessions = append(sessions, session)
	}

	return handlers.Response{
		Ctx:    c,
		Status: fiber.StatusOK,
		Data:   fiber.Map{"sessions": sessions},
	}.Send()
}

// Refresh
// @Summary refresh token
// @Description delete old token and creates a new user token
// @Tags auth
// @Produce json
// @Success 200 {object} handlers.ResponseData
// @Failure 400 {object} handlers.ResponseData
// @Failure 401 {object} handlers.ResponseData
// @Failure 500 {object} handlers.ResponseData
// @Security ApiKeyAuth
// @param Authorization header string true "Authorization"
// @Router /auth [put]
func Refresh(c *fiber.Ctx) (err error) {
	// Get user token information saved at data store
	tokenCache, err := jwt.GetTokenCache(c)
	if err != nil {
		return handlers.Response{
			Ctx:    c,
			Status: fiber.StatusUnauthorized,
			Msg:    "Invalid token",
			Data:   fiber.Map{"error": err.Error()},
		}.Send()
	}

	var user types.User
	user.ID = tokenCache.UserID
	user.Permits = tokenCache.UserPermits

	var si types.SessionInfo
	si.Browser = tokenCache.Browser
	si.IP = tokenCache.IP
	si.System = tokenCache.System

	// Generate encoded token
	t, err := jwt.GenerateToken(&user, viper.GetString("jwt.secret"), &si)
	if err != nil {
		return handlers.Response{
			Ctx:    c,
			Status: fiber.StatusInternalServerError,
			Msg:    "Error generating token",
			Data:   fiber.Map{"error": err.Error()},
		}.Send()
	}

	// Remove old token
	err = app.TokenCache.Del(jwt.GetReqTokenRaw(c)).Err()
	if err != nil {
		return handlers.Response{
			Ctx:    c,
			Status: fiber.StatusInternalServerError,
			Msg:    "Error deleting old token",
			Data:   fiber.Map{"error": err.Error()},
		}.Send()
	}

	return handlers.Response{
		Ctx:    c,
		Status: fiber.StatusOK,
		Data:   fiber.Map{"token": t},
	}.Send()
}

// Logout
// @Summary user logout
// @Description deletes user token
// @Tags auth
// @Produce json
// @Success 200 {object} handlers.ResponseData
// @Failure 400 {object} handlers.ResponseData
// @Failure 401 {object} handlers.ResponseData
// @Failure 500 {object} handlers.ResponseData
// @Security ApiKeyAuth
// @param Authorization header string true "Authorization"
// @Router /auth [delete]
func Logout(c *fiber.Ctx) (err error) {
	err = app.TokenCache.Del(jwt.GetReqTokenRaw(c)).Err()
	if err != nil {
		return handlers.Response{
			Ctx:    c,
			Status: fiber.StatusInternalServerError,
			Msg:    "Error deleting token",
			Data:   fiber.Map{"error": err.Error()},
		}.Send()
	}

	return handlers.Response{
		Ctx:    c,
		Status: fiber.StatusOK,
	}.Send()
}

// OTPGet
// @Summary get OTP
// @Description get user OTP key
// @Tags auth
// @Produce json
// @Success 200 {object} handlers.ResponseData
// @Failure 400 {object} handlers.ResponseData
// @Failure 500 {object} handlers.ResponseData
// @Security ApiKeyAuth
// @param Authorization header string true "Authorization"
// @Router /auth/otp [get]
func OTPGet(c *fiber.Ctx) (err error) {
	var user types.User

	// Get user from database
	if user, err = dal.PKUser(jwt.GetUserId(c)); err != nil {
		return handlers.Response{
			Ctx:    c,
			Status: fiber.StatusInternalServerError,
			Msg:    "Error to find User",
			Data:   fiber.Map{"error": err.Error()},
		}.Send()
	}

	// Generate a new OTP key
	key := jwt.GenerateOTP(user.Email)

	// Save user updated to database
	if err = dal.SetNewOTPKey(user, key.String()); err != nil {
		return handlers.Response{
			Ctx:    c,
			Status: fiber.StatusInternalServerError,
			Msg:    "Error to save user OTP key",
			Data:   fiber.Map{"error": err.Error()},
		}.Send()
	}

	// Get user token information saved at data store
	tokenCache, err := jwt.GetTokenCache(c)
	if err != nil {
		return handlers.Response{
			Ctx:    c,
			Status: fiber.StatusUnauthorized,
			Msg:    "Invalid token",
			Data:   fiber.Map{"error": err.Error()},
		}.Send()
	}

	var si types.SessionInfo
	si.Browser = tokenCache.Browser
	si.IP = tokenCache.IP
	si.System = tokenCache.System

	// Generate encoded token
	t, err := jwt.GenerateToken(&user, viper.GetString("jwt.otpSecret"), &si)
	if err != nil {
		return handlers.Response{
			Ctx:    c,
			Status: fiber.StatusInternalServerError,
			Msg:    "Error generating token",
			Data:   fiber.Map{"error": err.Error()},
		}.Send()
	}

	return handlers.Response{
		Ctx:    c,
		Status: fiber.StatusOK,
		Data:   fiber.Map{"qrcode": jwt.GenerateOTPQRCode(key), "otp": t},
	}.Send()
}

// OTPActivate
// @Summary activate OTP
// @Description activate user OTP key and return recovery codes
// @Tags auth
// @Produce json
// @Success 200 {object} handlers.ResponseData
// @Failure 400 {object} handlers.ResponseData
// @Failure 500 {object} handlers.ResponseData
// @Security ApiKeyAuth
// @param Authorization header string true "Authorization"
// @Router /auth/otp/:passcode [put]
func OTPActivate(c *fiber.Ctx) (err error) {
	passcode := c.Params("passcode")

	var user types.User

	// Get user from database
	if user, err = dal.PKUser(jwt.GetUserId(c)); err != nil {
		return handlers.Response{
			Ctx:    c,
			Status: fiber.StatusInternalServerError,
			Msg:    "Error to find User",
			Data:   fiber.Map{"error": err.Error()},
		}.Send()
	}

	// Validate the OTP key passcode
	isValid := jwt.ValidateOTP(user.OTPKey, passcode)
	if !isValid {
		return handlers.Response{
			Ctx:    c,
			Status: fiber.StatusInternalServerError,
			Msg:    "Error to validate OTP key passcode",
		}.Send()
	}

	// Validate that OTOPStatus is not already activated
	if user.OTPStatus {
		return handlers.Response{
			Ctx:    c,
			Status: fiber.StatusBadRequest,
			Msg:    "OTP key is already activated",
		}.Send()
	}

	// Generate recovery codes
	codes, err := json.Marshal(jwt.GenerateRecoveryCodes())
	if err != nil {
		return handlers.Response{
			Ctx:    c,
			Status: fiber.StatusInternalServerError,
			Msg:    "Error trying to generate recovery codes",
		}.Send()
	}
	user.OTPRecoveryCodes = datatypes.JSON(string(codes))

	// Save user otpStatus updated to database
	if err = dal.ActivateOTPKey(user); err != nil {
		return handlers.Response{
			Ctx:    c,
			Status: fiber.StatusInternalServerError,
			Msg:    "Error to activate OTP key at database",
			Data:   fiber.Map{"error": err.Error()},
		}.Send()
	}

	// Get user token information saved at data store
	tokenCache, err := jwt.GetTokenCache(c)
	if err != nil {
		return handlers.Response{
			Ctx:    c,
			Status: fiber.StatusUnauthorized,
			Msg:    "Invalid token",
			Data:   fiber.Map{"error": err.Error()},
		}.Send()
	}

	var si types.SessionInfo
	si.Browser = tokenCache.Browser
	si.IP = tokenCache.IP
	si.System = tokenCache.System

	// Generate encoded token and send it as response.
	t, err := jwt.GenerateToken(&user, viper.GetString("jwt.secret"), &si)
	if err != nil {
		return handlers.Response{
			Ctx:    c,
			Status: fiber.StatusInternalServerError,
			Msg:    "Error generating token",
			Data:   fiber.Map{"error": err.Error()},
		}.Send()
	}

	return handlers.Response{
		Ctx:    c,
		Status: fiber.StatusOK,
		Data:   fiber.Map{"recoveryCodes": user.OTPRecoveryCodes, "token": t},
	}.Send()
}

// OTPLogin
// @Summary OTP user login
// @Description validates OTP login information, creates token and send it in response
// @Tags auth
// @Accept json
// @Produce json
// @Param message body types.LoginRequest true "Login Info"
// @Success 200 {object} handlers.ResponseData
// @Failure 400 {object} handlers.ResponseData
// @Failure 401 {object} handlers.ResponseData
// @Failure 500 {object} handlers.ResponseData
// @Security ApiKeyAuth
// @param Authorization header string true "Authorization"
// @Router /auth/otp/:passcode [post]
func OTPLogin(c *fiber.Ctx) (err error) {
	passcode := c.Params("passcode")

	var user types.User

	// Get user from database
	if user, err = dal.PKUser(jwt.GetUserId(c)); err != nil {
		return handlers.Response{
			Ctx:    c,
			Status: fiber.StatusInternalServerError,
			Msg:    "Error to find User",
			Data:   fiber.Map{"error": err.Error()},
		}.Send()
	}

	// Retrieve OTP key from user
	key, err := otp.NewKeyFromURL(user.OTPKey)
	if err != nil {
		return handlers.Response{
			Ctx:    c,
			Status: fiber.StatusInternalServerError,
			Msg:    "Error retrieving OTP key",
			Data:   fiber.Map{"error": err.Error()},
		}.Send()
	}

	// Validate passcode
	isValid := totp.Validate(passcode, key.Secret())

	if !isValid {
		var codes []string
		err = json.Unmarshal([]byte(user.OTPRecoveryCodes), &codes)
		if err != nil {
			return handlers.Response{
				Ctx:    c,
				Status: fiber.StatusInternalServerError,
				Msg:    "Error trying to unmarshal recovery codes",
				Data:   fiber.Map{"error": err.Error()},
			}.Send()
		}

		for _, code := range codes {
			if code == passcode {
				isValid = true
			}
		}
	}

	if isValid {
		// Get user token information saved at data store
		tokenCache, err := jwt.GetTokenCache(c)
		if err != nil {
			return handlers.Response{
				Ctx:    c,
				Status: fiber.StatusUnauthorized,
				Msg:    "Invalid token",
				Data:   fiber.Map{"error": err.Error()},
			}.Send()
		}

		var si types.SessionInfo
		si.Browser = tokenCache.Browser
		si.IP = tokenCache.IP
		si.System = tokenCache.System

		// Generate encoded token and send it as response.
		t, err := jwt.GenerateToken(&user, viper.GetString("jwt.secret"), &si)
		if err != nil {
			return handlers.Response{
				Ctx:    c,
				Status: fiber.StatusInternalServerError,
				Msg:    "Error generating token",
				Data:   fiber.Map{"error": err.Error()},
			}.Send()
		}

		return handlers.Response{
			Ctx:    c,
			Status: fiber.StatusOK,
			Data:   fiber.Map{"token": t},
		}.Send()

	} else {
		return handlers.Response{
			Ctx:    c,
			Status: fiber.StatusUnauthorized,
			Msg:    "Error validating passcode",
		}.Send()
	}
}
