package services

import (
	"github.com/gofiber/fiber/v2"
	"gitlab.com/rodrigoodhin/fiper/pkgs/fgorm"
	"gitlab.com/rodrigoodhin/go-2fa-api/app"
	"gitlab.com/rodrigoodhin/go-2fa-api/internal/handlers"
)

// ListAll
// @Summary list all
// @Description retrieves all roles, permissions and roles permissions
// @Tags permit
// @Produce json
// @Success 200 {object} handlers.ResponseData
// @Failure 401 {object} handlers.ResponseData
// @Failure 500 {object} handlers.ResponseData
// @Security ApiKeyAuth
// @param Authorization header string true "Authorization"
// @Router /permit/list/all [get]
func ListAll(c *fiber.Ctx) (err error) {
	var (
		roles           []fgorm.FiperRole
		permissions     []fgorm.FiperPermission
		rolePermissions []fgorm.FiperRolePermission
	)

	if roles, err = app.FiPer.GetRoles(); err != nil {
		return handlers.Response{
			Ctx:    c,
			Status: fiber.StatusInternalServerError,
			Msg:    "Error to find Roles",
			Data:   fiber.Map{"error": err.Error()},
		}.Send()
	}

	if permissions, err = app.FiPer.GetPermissions(); err != nil {
		return handlers.Response{
			Ctx:    c,
			Status: fiber.StatusInternalServerError,
			Msg:    "Error to find Permissions",
			Data:   fiber.Map{"error": err.Error()},
		}.Send()
	}

	if rolePermissions, err = app.FiPer.GetRolePermissions(); err != nil {
		return handlers.Response{
			Ctx:    c,
			Status: fiber.StatusInternalServerError,
			Msg:    "Error to find Roles Permissions",
			Data:   fiber.Map{"error": err.Error()},
		}.Send()
	}

	return handlers.Response{
		Ctx:    c,
		Status: fiber.StatusOK,
		Data:   fiber.Map{"roles": roles, "permissions": permissions, "rolePermissions": rolePermissions},
	}.Send()
}

// ListRoles
// @Summary list roles
// @Description retrieves all roles
// @Tags permit
// @Produce json
// @Success 200 {object} handlers.ResponseData
// @Failure 401 {object} handlers.ResponseData
// @Failure 500 {object} handlers.ResponseData
// @Security ApiKeyAuth
// @param Authorization header string true "Authorization"
// @Router /permit/list/roles [get]
func ListRoles(c *fiber.Ctx) (err error) {
	var (
		roles []fgorm.FiperRole
	)

	if roles, err = app.FiPer.GetRoles(); err != nil {
		return handlers.Response{
			Ctx:    c,
			Status: fiber.StatusInternalServerError,
			Msg:    "Error to find Roles",
			Data:   fiber.Map{"error": err.Error()},
		}.Send()
	}

	return handlers.Response{
		Ctx:    c,
		Status: fiber.StatusOK,
		Data:   fiber.Map{"roles": roles},
	}.Send()
}

// ListPermissions
// @Summary list permissions
// @Description retrieves all permissions
// @Tags permit
// @Produce json
// @Success 200 {object} handlers.ResponseData
// @Failure 401 {object} handlers.ResponseData
// @Failure 500 {object} handlers.ResponseData
// @Security ApiKeyAuth
// @param Authorization header string true "Authorization"
// @Router /permit/list/permissions [get]
func ListPermissions(c *fiber.Ctx) (err error) {
	var (
		permissions []fgorm.FiperPermission
	)

	if permissions, err = app.FiPer.GetPermissions(); err != nil {
		return handlers.Response{
			Ctx:    c,
			Status: fiber.StatusInternalServerError,
			Msg:    "Error to find Permissions",
			Data:   fiber.Map{"error": err.Error()},
		}.Send()
	}

	return handlers.Response{
		Ctx:    c,
		Status: fiber.StatusOK,
		Data:   fiber.Map{"permissions": permissions},
	}.Send()
}

// ListRolesPermissions
// @Summary list roles permisisons
// @Description retrieves all roles permissions
// @Tags permit
// @Produce json
// @Success 200 {object} handlers.ResponseData
// @Failure 401 {object} handlers.ResponseData
// @Failure 500 {object} handlers.ResponseData
// @Security ApiKeyAuth
// @param Authorization header string true "Authorization"
// @Router /permit/list/rolesPermissions [get]
func ListRolesPermissions(c *fiber.Ctx) (err error) {
	var (
		rolePermissions []fgorm.FiperRolePermission
	)

	if rolePermissions, err = app.FiPer.GetRolePermissions(); err != nil {
		return handlers.Response{
			Ctx:    c,
			Status: fiber.StatusInternalServerError,
			Msg:    "Error to find Roles Permissions",
			Data:   fiber.Map{"error": err.Error()},
		}.Send()
	}

	return handlers.Response{
		Ctx:    c,
		Status: fiber.StatusOK,
		Data:   fiber.Map{"rolePermissions": rolePermissions},
	}.Send()
}

// AddRole
// @Summary add role
// @Description adds a role
// @Tags permit
// @Accept json
// @Produce json
// @Param message body fgorm.FiperRole true "Role Info"
// @Success 200 {object} handlers.ResponseData
// @Failure 400 {object} handlers.ResponseData
// @Failure 401 {object} handlers.ResponseData
// @Failure 500 {object} handlers.ResponseData
// @Security ApiKeyAuth
// @param Authorization header string true "Authorization"
// @Router /permit/add/role [post]
func AddRole(c *fiber.Ctx) (err error) {
	var role fgorm.FiperRole

	if err = c.BodyParser(&role); err != nil {
		return handlers.Response{
			Ctx:    c,
			Status: fiber.StatusBadRequest,
			Msg:    "Error to parse request body",
			Data:   fiber.Map{"error": err.Error()},
		}.Send()
	}

	role, err = app.FiPer.AddRole(role)
	if err != nil {
		return handlers.Response{
			Ctx:    c,
			Status: fiber.StatusBadRequest,
			Msg:    "Error to create Role",
			Data:   fiber.Map{"error": err.Error()},
		}.Send()
	}

	return handlers.Response{
		Ctx:    c,
		Status: fiber.StatusOK,
		Data:   fiber.Map{"role": role},
	}.Send()
}

// AddPermission
// @Summary add permission
// @Description adds a permission
// @Tags permit
// @Accept json
// @Produce json
// @Param message body fgorm.FiperPermission true "Permission Info"
// @Success 200 {object} handlers.ResponseData
// @Failure 400 {object} handlers.ResponseData
// @Failure 401 {object} handlers.ResponseData
// @Failure 500 {object} handlers.ResponseData
// @Security ApiKeyAuth
// @param Authorization header string true "Authorization"
// @Router /permit/add/permission [post]
func AddPermission(c *fiber.Ctx) (err error) {
	var permission fgorm.FiperPermission

	if err = c.BodyParser(&permission); err != nil {
		return handlers.Response{
			Ctx:    c,
			Status: fiber.StatusBadRequest,
			Msg:    "Error to parse request body",
			Data:   fiber.Map{"error": err.Error()},
		}.Send()
	}

	permission, err = app.FiPer.AddPermission(permission)
	if err != nil {
		return handlers.Response{
			Ctx:    c,
			Status: fiber.StatusBadRequest,
			Msg:    "Error to create Permission",
			Data:   fiber.Map{"error": err.Error()},
		}.Send()
	}

	return handlers.Response{
		Ctx:    c,
		Status: fiber.StatusOK,
		Data:   fiber.Map{"permission": permission},
	}.Send()
}

// AddRolePermission
// @Summary add role permission
// @Description adds a role permission
// @Tags permit
// @Accept json
// @Produce json
// @Param message body fgorm.FiperRolePermission true "RolePermission Info"
// @Success 200 {object} handlers.ResponseData
// @Failure 400 {object} handlers.ResponseData
// @Failure 401 {object} handlers.ResponseData
// @Failure 500 {object} handlers.ResponseData
// @Security ApiKeyAuth
// @param Authorization header string true "Authorization"
// @Router /permit/add/rolePermission [post]
func AddRolePermission(c *fiber.Ctx) (err error) {
	var (
		arp            fgorm.AddRolePermission
		rolePermission fgorm.FiperRolePermission
	)

	if err = c.BodyParser(&arp); err != nil {
		return handlers.Response{
			Ctx:    c,
			Status: fiber.StatusBadRequest,
			Msg:    "Error to parse request body",
			Data:   fiber.Map{"error": err.Error()},
		}.Send()
	}

	rolePermission, err = app.FiPer.AddRolePermission(arp)
	if err != nil {
		return handlers.Response{
			Ctx:    c,
			Status: fiber.StatusBadRequest,
			Msg:    "Error to create Role Permission",
			Data:   fiber.Map{"error": err.Error()},
		}.Send()
	}

	return handlers.Response{
		Ctx:    c,
		Status: fiber.StatusOK,
		Data:   fiber.Map{"rolePermission": rolePermission},
	}.Send()
}

// DeleteRole
// @Summary delete role
// @Description deletes a role
// @Tags permit
// @Produce json
// @Param name path string true "Role Name"
// @Success 200 {object} handlers.ResponseData
// @Failure 400 {object} handlers.ResponseData
// @Failure 401 {object} handlers.ResponseData
// @Failure 500 {object} handlers.ResponseData
// @Security ApiKeyAuth
// @param Authorization header string true "Authorization"
// @Router /permit/delete/role/:name [delete]
func DeleteRole(c *fiber.Ctx) (err error) {
	err = app.FiPer.DeleteRole(c.Params("name"))
	if err != nil {
		return handlers.Response{
			Ctx:    c,
			Status: fiber.StatusInternalServerError,
			Msg:    "Error to delete Role",
			Data:   fiber.Map{"error": err.Error()},
		}.Send()
	}

	return handlers.Response{
		Ctx:    c,
		Status: fiber.StatusOK,
	}.Send()
}

// DeletePermission
// @Summary delete permission
// @Description deletes a permission
// @Tags permit
// @Produce json
// @Param model path string true "Permission Model"
// @Param action path string true "Permission Action"
// @Success 200 {object} handlers.ResponseData
// @Failure 400 {object} handlers.ResponseData
// @Failure 401 {object} handlers.ResponseData
// @Failure 500 {object} handlers.ResponseData
// @Security ApiKeyAuth
// @param Authorization header string true "Authorization"
// @Router /permit/delete/permission/:model/:action [delete]
func DeletePermission(c *fiber.Ctx) (err error) {
	err = app.FiPer.DeletePermission(c.Params("model"), c.Params("action"))
	if err != nil {
		return handlers.Response{
			Ctx:    c,
			Status: fiber.StatusInternalServerError,
			Msg:    "Error to delete Permission",
			Data:   fiber.Map{"error": err.Error()},
		}.Send()
	}

	return handlers.Response{
		Ctx:    c,
		Status: fiber.StatusOK,
	}.Send()
}

// DeleteRolePermision
// @Summary delete role permission
// @Description deletes a role permission
// @Tags permit
// @Produce json
// @Param name path string true "Role Name"
// @Param model path string true "Permission Model"
// @Param action path string true "Permission Action"
// @Success 200 {object} handlers.ResponseData
// @Failure 400 {object} handlers.ResponseData
// @Failure 401 {object} handlers.ResponseData
// @Failure 500 {object} handlers.ResponseData
// @Security ApiKeyAuth
// @param Authorization header string true "Authorization"
// @Router /permit/delete/rolePermission/:name/:model/:action [delete]
func DeleteRolePermission(c *fiber.Ctx) (err error) {
	err = app.FiPer.DeleteRolePermission(c.Params("name"), c.Params("model"), c.Params("action"))
	if err != nil {
		return handlers.Response{
			Ctx:    c,
			Status: fiber.StatusInternalServerError,
			Msg:    "Error to delete Role Permission",
			Data:   fiber.Map{"error": err.Error()},
		}.Send()
	}

	return handlers.Response{
		Ctx:    c,
		Status: fiber.StatusOK,
	}.Send()
}
