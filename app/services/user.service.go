package services

import (
	"strings"
	"time"

	"github.com/gofiber/fiber/v2"
	"github.com/raja/argon2pw"
	"github.com/spf13/viper"
	"gitlab.com/rodrigoodhin/fiper/pkgs/fgorm"
	"gitlab.com/rodrigoodhin/go-2fa-api/app"
	"gitlab.com/rodrigoodhin/go-2fa-api/app/dal"
	"gitlab.com/rodrigoodhin/go-2fa-api/app/types"
	"gitlab.com/rodrigoodhin/go-2fa-api/app/utils"
	"gitlab.com/rodrigoodhin/go-2fa-api/app/utils/jwt"
	"gitlab.com/rodrigoodhin/go-2fa-api/internal/handlers"
	"gitlab.com/rodrigoodhin/go-2fa-api/internal/helpers"
	"gitlab.com/rodrigoodhin/go-2fa-api/internal/notifications/email"
)

// ListUsers
// @Summary list users
// @Description retrieves all users
// @Tags user
// @Produce json
// @Success 200 {object} handlers.ResponseData
// @Failure 400 {object} handlers.ResponseData
// @Failure 401 {object} handlers.ResponseData
// @Security ApiKeyAuth
// @param Authorization header string true "Authorization"
// @Router /user [get]
func ListUsers(c *fiber.Ctx) (err error) {
	var users []types.UserResponse

	if users, err = dal.ListUsers(); err != nil {
		return handlers.Response{
			Ctx:    c,
			Status: fiber.StatusInternalServerError,
			Msg:    "Error to find Users",
			Data:   fiber.Map{"error": err.Error()},
		}.Send()
	}

	return handlers.Response{
		Ctx:    c,
		Status: fiber.StatusOK,
		Data:   fiber.Map{"users": users},
	}.Send()
}

// GetUser
// @Summary get user
// @Description retrieves a user
// @Tags user
// @Produce json
// @Param id path string true "User ID"
// @Success 200 {object} handlers.ResponseData
// @Failure 400 {object} handlers.ResponseData
// @Failure 500 {object} handlers.ResponseData
// @Security ApiKeyAuth
// @param Authorization header string true "Authorization"
// @Router /user/:id [get]
func GetUser(c *fiber.Ctx) (err error) {
	var user types.User

	if user, err = dal.PKUser(c.Params("id")); err != nil {
		return handlers.Response{
			Ctx:    c,
			Status: fiber.StatusInternalServerError,
			Msg:    "Error to find User",
			Data:   fiber.Map{"error": err.Error()},
		}.Send()
	}

	return handlers.Response{
		Ctx:    c,
		Status: fiber.StatusOK,
		Data:   fiber.Map{"user": utils.CreateResponseUser(user)},
	}.Send()
}

// UpsertUser
// @Summary add or update user
// @Description adds or updates a user
// @Tags user
// @Accept json
// @Produce json
// @Param message body types.UserUpsert true "User Info"
// @Success 200 {object} handlers.ResponseData
// @Failure 400 {object} handlers.ResponseData
// @Failure 401 {object} handlers.ResponseData
// @Failure 500 {object} handlers.ResponseData
// @Security ApiKeyAuth
// @param Authorization header string true "Authorization"
// @Router /user [put]
func UpsertUser(c *fiber.Ctx) (err error) {
	var (
		user       types.User
		userUpsert types.UserUpsert
	)

	if err = c.BodyParser(&userUpsert); err != nil {
		return handlers.Response{
			Ctx:    c,
			Status: fiber.StatusBadRequest,
			Msg:    "Error to parse request body",
			Data:   fiber.Map{"error": err.Error()},
		}.Send()
	}

	if err = userUpsert.Validate(); err != nil {
		return handlers.Response{
			Ctx:    c,
			Status: fiber.StatusInternalServerError,
			Msg:    "Error to validate User",
			Data:   fiber.Map{"error": err.Error()},
		}.Send()
	}

	if userUpsert.Password != "" {

		if err = userUpsert.ValidatePassword(); err != nil {
			return handlers.Response{
				Ctx:    c,
				Status: fiber.StatusInternalServerError,
				Msg:    "Error to validate User password",
				Data:   fiber.Map{"error": err.Error()},
			}.Send()
		}

		if isValid, msgs, errors := helpers.CheckPassStrength(userUpsert.Password); !isValid {
			return handlers.Response{
				Ctx:    c,
				Status: fiber.StatusInternalServerError,
				Msg:    "Error checking password strength",
				Data:   fiber.Map{"msg": strings.Join(msgs, ", "), "error": strings.Join(errors, ", ")},
			}.Send()
		}

		if userUpsert.Password, err = argon2pw.GenerateSaltedHash(userUpsert.Password); err != nil {
			return handlers.Response{
				Ctx:    c,
				Status: fiber.StatusInternalServerError,
				Msg:    "Hash generated returned error",
				Data:   fiber.Map{"error": err.Error()},
			}.Send()
		}

	}

	if user, err = dal.UpsertUser(userUpsert); err != nil {
		return handlers.Response{
			Ctx:    c,
			Status: fiber.StatusInternalServerError,
			Msg:    "Error to upsert User",
			Data:   fiber.Map{"error": err.Error()},
		}.Send()
	}

	return handlers.Response{
		Ctx:    c,
		Status: fiber.StatusOK,
		Data:   fiber.Map{"user": utils.CreateResponseUser(user)},
	}.Send()
}

// DeleteUser
// @Summary delete user
// @Description deletes a user
// @Tags user
// @Produce json
// @Param id path string true "User ID"
// @Success 200 {object} handlers.ResponseData
// @Failure 400 {object} handlers.ResponseData
// @Failure 500 {object} handlers.ResponseData
// @Security ApiKeyAuth
// @param Authorization header string true "Authorization"
// @Router /user/:id [delete]
func DeleteUser(c *fiber.Ctx) (err error) {
	if err = dal.DeleteUser(c.Params("id")); err != nil {
		return handlers.Response{
			Ctx:    c,
			Status: fiber.StatusInternalServerError,
			Msg:    "Error to delete User",
			Data:   fiber.Map{"error": err.Error()},
		}.Send()
	}

	return handlers.Response{
		Ctx:    c,
		Status: fiber.StatusOK,
	}.Send()
}

// UserPassReset
// @Summary user password reset
// @Description reset user password and send an email with a link to create a new one
// @Tags user
// @Accept json
// @Produce json
// @Param message body types.PassResetRequest true "Request Info"
// @Success 200 {object} handlers.ResponseData
// @Failure 400 {object} handlers.ResponseData
// @Failure 401 {object} handlers.ResponseData
// @Failure 500 {object} handlers.ResponseData
// @Router /user/password/reset [post]
func UserPassReset(c *fiber.Ctx) (err error) {
	var (
		user             types.User
		passResetRequest types.PassResetRequest
	)

	err = c.BodyParser(&passResetRequest)
	if err != nil {
		return handlers.Response{
			Ctx:    c,
			Status: fiber.StatusBadRequest,
			Msg:    "Request body is invalid",
			Data:   fiber.Map{"error": err.Error()},
		}.Send()
	}

	// Clear all user login info
	if user, err = dal.PassResetRequest(passResetRequest.Email); err != nil {
		return handlers.Response{
			Ctx:    c,
			Status: fiber.StatusInternalServerError,
			Msg:    "Error to clear user login info",
			Data:   fiber.Map{"error": err.Error()},
		}.Send()
	}

	var si types.SessionInfo
	si.Browser = passResetRequest.Browser
	si.IP = passResetRequest.IP
	si.System = passResetRequest.System

	// Generate encoded password reset token
	token, err := jwt.GenerateToken(&user, viper.GetString("jwt.passResetSecret"), &si)
	if err != nil {
		return handlers.Response{
			Ctx:    c,
			Status: fiber.StatusInternalServerError,
			Msg:    "Error generating token",
			Data:   fiber.Map{"error": err.Error()},
		}.Send()
	}

	// Prepare password reset info
	passwordResetHTML := types.PassResetHTML{
		App:             viper.GetString("app"),
		AppUrl:          viper.GetString("fullUrl"),
		LogoBase64:      viper.GetString("email.logoBase64"),
		Username:        user.Username,
		Email:           user.Email,
		OperatingSystem: passResetRequest.System,
		BrowserName:     passResetRequest.Browser,
		ActionURL:       viper.GetString("fullUrl") + viper.GetString("email.userPassResetActionPath") + token,
		CurrentYear:     time.Now().Year(),
	}

	// Send password reset email
	err = email.SendPasswordResetEmail(&passwordResetHTML)
	if err != nil {
		return handlers.Response{
			Ctx:    c,
			Status: fiber.StatusInternalServerError,
			Msg:    "Error sending password reset email",
			Data:   fiber.Map{"error": err.Error()},
		}.Send()
	}

	return handlers.Response{
		Ctx:    c,
		Status: fiber.StatusOK,
	}.Send()
}

// UserPassResetConfirm
// @Summary user password reset confirm
// @Description confirm new User password
// @Tags user
// @Accept json
// @Produce json
// @Param message body types.PassResetRequest true "Request Info"
// @Success 200 {object} handlers.ResponseData
// @Failure 400 {object} handlers.ResponseData
// @Failure 401 {object} handlers.ResponseData
// @Failure 500 {object} handlers.ResponseData
// @Security ApiKeyAuth
// @param Authorization header string true "Authorization"
// @Router /user/password/reset [put]
func UserPassResetConfirm(c *fiber.Ctx) (err error) {
	var passResetRequest types.PassResetRequest

	err = c.BodyParser(&passResetRequest)
	if err != nil {
		return handlers.Response{
			Ctx:    c,
			Status: fiber.StatusBadRequest,
			Msg:    "Request body is invalid",
			Data:   fiber.Map{"error": err.Error()},
		}.Send()
	}

	// Validate new password
	if err = passResetRequest.Validate(); err != nil {
		return handlers.Response{
			Ctx:    c,
			Status: fiber.StatusInternalServerError,
			Msg:    "Error to validate new password",
			Data:   fiber.Map{"error": err.Error()},
		}.Send()
	}

	// Check if password and password confirmation are equals
	if passResetRequest.Password != passResetRequest.PasswordConfirm {
		return handlers.Response{
			Ctx:    c,
			Status: fiber.StatusInternalServerError,
			Msg:    "Passwords are different",
		}.Send()
	}

	if passResetRequest.Password, err = argon2pw.GenerateSaltedHash(passResetRequest.Password); err != nil {
		return handlers.Response{
			Ctx:    c,
			Status: fiber.StatusInternalServerError,
			Msg:    "Hash generated returned error",
			Data:   fiber.Map{"error": err.Error()},
		}.Send()
	}

	// Save password update to db
	if err = dal.PassResetConfirm(jwt.GetUserId(c), passResetRequest.Password); err != nil {
		return handlers.Response{
			Ctx:    c,
			Status: fiber.StatusInternalServerError,
			Msg:    "Error to update user password",
			Data:   fiber.Map{"error": err.Error()},
		}.Send()
	}

	// Delete token
	err = app.TokenCache.Del(jwt.GetReqTokenRaw(c)).Err()
	if err != nil {
		return handlers.Response{
			Ctx:    c,
			Status: fiber.StatusInternalServerError,
			Msg:    "Error deleting token",
			Data:   fiber.Map{"error": err.Error()},
		}.Send()
	}

	return handlers.Response{
		Ctx:    c,
		Status: fiber.StatusOK,
	}.Send()
}

// UserPassChange
// @Summary user password change
// @Description confirm new User password
// @Tags user
// @Accept json
// @Produce json
// @Param id path string true "User ID"
// @Param message body types.PassChangeRequest true "Request Info"
// @Success 200 {object} handlers.ResponseData
// @Failure 400 {object} handlers.ResponseData
// @Failure 401 {object} handlers.ResponseData
// @Failure 500 {object} handlers.ResponseData
// @Security ApiKeyAuth
// @param Authorization header string true "Authorization"
// @Router /user/password/change/:id [put]
func UserPassChange(c *fiber.Ctx) (err error) {
	var passChangeRequest types.PassChangeRequest

	err = c.BodyParser(&passChangeRequest)
	if err != nil {
		return handlers.Response{
			Ctx:    c,
			Status: fiber.StatusBadRequest,
			Msg:    "Request body is invalid",
			Data:   fiber.Map{"error": err.Error()},
		}.Send()
	}

	// Validate new password
	if err = passChangeRequest.Validate(); err != nil {
		return handlers.Response{
			Ctx:    c,
			Status: fiber.StatusInternalServerError,
			Msg:    "Error to validate new password",
			Data:   fiber.Map{"error": err.Error()},
		}.Send()
	}

	// Check if password and password confirmation are equals
	if passChangeRequest.Password != passChangeRequest.PasswordConfirm {
		return handlers.Response{
			Ctx:    c,
			Status: fiber.StatusInternalServerError,
			Msg:    "Passwords are different",
		}.Send()
	}

	// Check password strength
	if isValid, msgs, errors := helpers.CheckPassStrength(passChangeRequest.Password); !isValid {
		return handlers.Response{
			Ctx:    c,
			Status: fiber.StatusInternalServerError,
			Msg:    "Error checking password strength",
			Data:   fiber.Map{"msg": strings.Join(msgs, ", "), "error": strings.Join(errors, ", ")},
		}.Send()
	}

	// Check current password and Save password update to db
	if err = dal.PassChange(passChangeRequest.CurrentPassword, passChangeRequest.Password, c.Params("id")); err != nil {
		return handlers.Response{
			Ctx:    c,
			Status: fiber.StatusInternalServerError,
			Msg:    "Error to change user password",
			Data:   fiber.Map{"error": err.Error()},
		}.Send()
	}

	// Delete token
	err = app.TokenCache.Del(jwt.GetReqTokenRaw(c)).Err()
	if err != nil {
		return handlers.Response{
			Ctx:    c,
			Status: fiber.StatusInternalServerError,
			Msg:    "Error deleting token",
			Data:   fiber.Map{"error": err.Error()},
		}.Send()
	}

	return handlers.Response{
		Ctx:    c,
		Status: fiber.StatusOK,
	}.Send()
}

// GetUserPermits
// @Summary get user permits
// @Description retrieves user permits
// @Tags user
// @Produce json
// @Param id path string true "User ID"
// @Success 200 {object} handlers.ResponseData
// @Failure 400 {object} handlers.ResponseData
// @Failure 500 {object} handlers.ResponseData
// @Security ApiKeyAuth
// @param Authorization header string true "Authorization"
// @Router /user/permits/:id [get]
func GetUserPermits(c *fiber.Ctx) (err error) {
	var permits fgorm.Permits

	if permits, err = app.FiPer.GetUserPermits(c.Params("id")); err != nil {
		return handlers.Response{
			Ctx:    c,
			Status: fiber.StatusInternalServerError,
			Msg:    "Error to find User permits",
			Data:   fiber.Map{"error": err.Error()},
		}.Send()
	}

	return handlers.Response{
		Ctx:    c,
		Status: fiber.StatusOK,
		Data:   fiber.Map{"permits": permits},
	}.Send()
}

// GrantUserRole
// @Summary grant user role
// @Description grants user role
// @Tags user
// @Accept json
// @Produce json
// @Param message body types.Permit true "Permits Info"
// @Success 200 {object} handlers.ResponseData
// @Failure 400 {object} handlers.ResponseData
// @Failure 401 {object} handlers.ResponseData
// @Failure 500 {object} handlers.ResponseData
// @Security ApiKeyAuth
// @param Authorization header string true "Authorization"
// @Router /user/grant/role [post]
func GrantUserRole(c *fiber.Ctx) (err error) {
	var (
		permits     types.PermitRole
		userPermits fgorm.Permits
	)

	if err = c.BodyParser(&permits); err != nil {
		return handlers.Response{
			Ctx:    c,
			Status: fiber.StatusBadRequest,
			Msg:    "Error to parse request body",
			Data:   fiber.Map{"error": err.Error()},
		}.Send()
	}

	if userPermits, err = app.FiPer.GrantUserRole(permits.UserID, permits.Name); err != nil {
		return handlers.Response{
			Ctx:    c,
			Status: fiber.StatusInternalServerError,
			Msg:    "Error to grant User role",
			Data:   fiber.Map{"error": err.Error()},
		}.Send()
	}

	return handlers.Response{
		Ctx:    c,
		Status: fiber.StatusOK,
		Data:   fiber.Map{"permits": userPermits},
	}.Send()
}

// GrantUserPermission
// @Summary grant user permission
// @Description grants user permission
// @Tags user
// @Accept json
// @Produce json
// @Param message body types.Permit true "Permits Info"
// @Success 200 {object} handlers.ResponseData
// @Failure 400 {object} handlers.ResponseData
// @Failure 401 {object} handlers.ResponseData
// @Failure 500 {object} handlers.ResponseData
// @Security ApiKeyAuth
// @param Authorization header string true "Authorization"
// @Router /user/grant/permission [post]
func GrantUserPermission(c *fiber.Ctx) (err error) {
	var (
		permits     types.PermitPermission
		userPermits fgorm.Permits
	)

	if err = c.BodyParser(&permits); err != nil {
		return handlers.Response{
			Ctx:    c,
			Status: fiber.StatusBadRequest,
			Msg:    "Error to parse request body",
			Data:   fiber.Map{"error": err.Error()},
		}.Send()
	}

	if userPermits, err = app.FiPer.GrantUserPermission(permits.UserID, permits.Model, permits.Action); err != nil {
		return handlers.Response{
			Ctx:    c,
			Status: fiber.StatusInternalServerError,
			Msg:    "Error to grant User permission",
			Data:   fiber.Map{"error": err.Error()},
		}.Send()
	}

	return handlers.Response{
		Ctx:    c,
		Status: fiber.StatusOK,
		Data:   fiber.Map{"permits": userPermits},
	}.Send()
}

// RevokeUserPermit
// @Summary revoke user permit
// @Description revokes user permit
// @Tags user
// @Accept json
// @Produce json
// @Param message body types.Permit true "Permits Info"
// @Success 200 {object} handlers.ResponseData
// @Failure 400 {object} handlers.ResponseData
// @Failure 401 {object} handlers.ResponseData
// @Failure 500 {object} handlers.ResponseData
// @Security ApiKeyAuth
// @param Authorization header string true "Authorization"
// @Router /user/revoke/permit [delete]
func RevokeUserPermit(c *fiber.Ctx) (err error) {
	var (
		permits     types.Permit
		userPermits fgorm.Permits
	)

	if err = c.BodyParser(&permits); err != nil {
		return handlers.Response{
			Ctx:    c,
			Status: fiber.StatusBadRequest,
			Msg:    "Error to parse request body",
			Data:   fiber.Map{"error": err.Error()},
		}.Send()
	}

	if userPermits, err = app.FiPer.RevokeUserPermit(permits.UserID, permits.Permit); err != nil {
		return handlers.Response{
			Ctx:    c,
			Status: fiber.StatusInternalServerError,
			Msg:    "Error to revoke User permit",
			Data:   fiber.Map{"error": err.Error()},
		}.Send()
	}

	return handlers.Response{
		Ctx:    c,
		Status: fiber.StatusOK,
		Data:   fiber.Map{"permits": userPermits},
	}.Send()
}
