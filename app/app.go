package app

import (
	"github.com/go-redis/redis"
	"github.com/gofiber/fiber/v2"
	"gitlab.com/rodrigoodhin/fiper/pkgs/fgorm"
)

var (
	Fiber      *fiber.App
	FiPer      *fgorm.FiPer
	TokenCache *redis.Client
)
