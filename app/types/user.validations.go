package types

import (
	validation "github.com/go-ozzo/ozzo-validation/v4"
	"gitlab.com/rodrigoodhin/go-2fa-api/internal/rules"
)

// Validate User
func (a User) Validate() error {
	return validation.ValidateStruct(&a,
		validation.Field(&a.Username, rules.UsernameRule...),
		validation.Field(&a.Password, rules.PasswordRule...),
		validation.Field(&a.Email, rules.EmailRule...),
		validation.Field(&a.Status, rules.StatusRule...),
		validation.Field(&a.UserType, rules.UserTypeRule...),
	)
}

// Validate UserUpsert
func (a UserUpsert) Validate() error {
	return validation.ValidateStruct(&a,
		validation.Field(&a.Username, rules.UsernameRule...),
		validation.Field(&a.Email, rules.EmailRule...),
		validation.Field(&a.UserType, rules.UserTypeRule...),
	)
}

// Validate UserUpsert Password
func (a UserUpsert) ValidatePassword() error {
	return validation.ValidateStruct(&a,
		validation.Field(&a.Password, rules.PasswordRule...),
	)
}

// Validate PassChangeRequest
func (a PassChangeRequest) Validate() error {
	return validation.ValidateStruct(&a,
		validation.Field(&a.Password, rules.PasswordRule...),
	)
}

// Validate PassResetRequest
func (a PassResetRequest) Validate() error {
	return validation.ValidateStruct(&a,
		validation.Field(&a.Password, rules.PasswordRule...),
	)
}
