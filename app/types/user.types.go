package types

import (
	"github.com/google/uuid"
	"gitlab.com/rodrigoodhin/go-2fa-api/internal/database"
	"gorm.io/datatypes"
)

// User - User table model
type User struct {
	database.Model
	Username         string         `json:"username" gorm:"not null;unique"`
	Password         string         `json:"password"`
	Email            string         `json:"email" gorm:"not null;unique"`
	Status           bool           `json:"status" gorm:"not null"`
	Permits          datatypes.JSON `json:"permits" gorm:"type:json"`
	OTPKey           string         `json:"otpKey"`
	OTPStatus        bool           `json:"otpStatus"`
	OTPRecoveryCodes datatypes.JSON `json:"otpRecoveryCodes"`
	UserType         string         `json:"user_type" gorm:"not null;default:'employee'"`
}

// UserUpsert - User upsert data
type UserUpsert struct {
	ID       uuid.UUID      `json:"id"`
	Username string         `json:"username"`
	Password string         `json:"password"`
	Email    string         `json:"email"`
	Status   bool           `json:"status"`
	Permits  datatypes.JSON `json:"permits"`
	UserType string         `json:"user_type"`
}

// UserResponse -  User Response Data
type UserResponse struct {
	ID       uuid.UUID      `json:"id"`
	Username string         `json:"username"`
	Email    string         `json:"email"`
	Status   bool           `json:"status"`
	Permits  datatypes.JSON `json:"permits"`
	UserType string         `json:"user_type"`
}

// PassChangeRequest -  Password Change Request Data
type PassChangeRequest struct {
	CurrentPassword string `json:"currentPassword"`
	Password        string `json:"password"`
	PasswordConfirm string `json:"passwordConfirm"`
}

// PassResetHTML - Password reset email template fields
type PassResetHTML struct {
	App             string
	AppUrl          string
	LogoBase64      string
	Username        string
	Email           string
	OperatingSystem string
	BrowserName     string
	ActionURL       string
	CurrentYear     int
}

// PassResetRequest -  Password Reset Request Data
type PassResetRequest struct {
	Email           string `json:"email"`
	System          string `json:"system"`
	Browser         string `json:"browser"`
	IP              string `json:"ip"`
	Password        string `json:"password"`
	PasswordConfirm string `json:"passwordConfirm"`
}

// ProfileResponse -  Profile Response Data
type ProfileResponse struct {
	SessionKey   string     `json:"sessionKey"`
	SessionValue TokenCache `json:"sessionValue"`
}
