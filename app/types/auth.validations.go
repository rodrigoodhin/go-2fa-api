package types

import (
	validation "github.com/go-ozzo/ozzo-validation/v4"
	"gitlab.com/rodrigoodhin/go-2fa-api/internal/rules"
)

// Validate LoginRequest
func (a LoginRequest) Validate() error {
	return validation.ValidateStruct(&a,
		validation.Field(&a.Username, rules.UsernameRule...),
		validation.Field(&a.Password, rules.PasswordRule...),
	)
}
