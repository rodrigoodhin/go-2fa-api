package types

// Permit -  Permit Data
type Permit struct {
	UserID string `json:"user_id"`
	Permit string `json:"permit"`
}

// PermitRole -  Permit Role Data
type PermitRole struct {
	UserID string `json:"user_id"`
	Name   string `json:"name"`
}

// PermitPermission -  Permit Permission Data
type PermitPermission struct {
	UserID string `json:"user_id"`
	Model  string `json:"model"`
	Action string `json:"action"`
}

