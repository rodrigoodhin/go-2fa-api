package types

import (
	"time"

	"github.com/google/uuid"
	"gorm.io/datatypes"
)

// LoginRequest -  Login Request Data
type LoginRequest struct {
	Username string `json:"username"`
	Password string `json:"password"`
	Browser  string `json:"browser"`
	IP       string `json:"ip"`
	System   string `json:"system"`
}

// LoginResponse - Login Response Data
type LoginResponse struct {
	Token string `json:"token"`
}

// SessionInfo -  Sessin info Data
type SessionInfo struct {
	Browser string
	IP      string
	System  string
}

// TokenCache - Token Cache fields
type TokenCache struct {
	UserID      uuid.UUID      `json:"userID"`
	UserPermits datatypes.JSON `json:"userPermits"`
	CreatedAt   time.Time      `json:"createdAt"`
	ExpireAt    time.Time      `json:"expireAt"`
	Browser     string         `json:"browser"`
	IP          string         `json:"ip"`
	System      string         `json:"system"`
}
