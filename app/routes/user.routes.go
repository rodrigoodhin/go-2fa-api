package routes

import (
	"github.com/gofiber/fiber/v2"
	"gitlab.com/rodrigoodhin/go-2fa-api/app"
	"gitlab.com/rodrigoodhin/go-2fa-api/app/services"
	"gitlab.com/rodrigoodhin/go-2fa-api/app/utils/middleware"
)

func UserRoutes(router fiber.Router) fiber.Router {

	router.Get("/",
		middleware.IsRequired(),
		app.FiPer.CheckFiPer([]string{"admin", "user:list"}),
		services.ListUsers)

	router.Get("/:id",
		middleware.IsRequired(),
		app.FiPer.CheckFiPer([]string{"admin", "user:get"}),
		services.GetUser)

	router.Put("/",
		middleware.IsRequired(),
		app.FiPer.CheckFiPer([]string{"admin", "user:upsert"}),
		services.UpsertUser)

	router.Delete("/:id",
		middleware.IsRequired(),
		app.FiPer.CheckFiPer([]string{"admin", "user:delete"}),
		services.DeleteUser)

	router.Post("/password/reset",
		services.UserPassReset)

	router.Put("/password/reset",
		middleware.IsRequiredPassReset(),
		app.FiPer.CheckFiPer([]string{"admin", "user:passResetConfirm"}),
		services.UserPassResetConfirm)

	router.Put("/password/change/:id",
		middleware.IsRequired(),
		app.FiPer.CheckFiPer([]string{"admin", "user:passChange"}),
		services.UserPassChange)

	router.Get("/permits/:id",
		middleware.IsRequired(),
		app.FiPer.CheckFiPer([]string{"admin", "user:getPermits"}),
		services.GetUserPermits)

	router.Post("/grant/role",
		middleware.IsRequired(),
		app.FiPer.CheckFiPer([]string{"admin", "user:grantRole"}),
		services.GrantUserRole)

	router.Post("/grant/permission",
		middleware.IsRequired(),
		app.FiPer.CheckFiPer([]string{"admin", "user:grantPermission"}),
		services.GrantUserPermission)

	router.Delete("/revoke/permit",
		middleware.IsRequired(),
		app.FiPer.CheckFiPer([]string{"admin", "user:revokePermit"}),
		services.RevokeUserPermit)

	return router
}
