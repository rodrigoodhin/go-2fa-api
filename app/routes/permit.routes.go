package routes

import (
	"github.com/gofiber/fiber/v2"
	"gitlab.com/rodrigoodhin/go-2fa-api/app"
	"gitlab.com/rodrigoodhin/go-2fa-api/app/services"
	"gitlab.com/rodrigoodhin/go-2fa-api/app/utils/middleware"
)

func PermitRoutes(router fiber.Router) fiber.Router {

	router.Get("/list/all",
		middleware.IsRequired(),
		app.FiPer.CheckFiPer([]string{"admin", "role:listAll"}),
		services.ListAll)

	router.Get("/list/roles",
		middleware.IsRequired(),
		app.FiPer.CheckFiPer([]string{"admin", "role:listRoles"}),
		services.ListRoles)

	router.Get("/list/permissions",
		middleware.IsRequired(),
		app.FiPer.CheckFiPer([]string{"admin", "role:listPermissions"}),
		services.ListPermissions)

	router.Get("/list/rolesPermissions",
		middleware.IsRequired(),
		app.FiPer.CheckFiPer([]string{"admin", "role:listRolesPermissions"}),
		services.ListRolesPermissions)

	router.Post("/add/role",
		middleware.IsRequired(),
		app.FiPer.CheckFiPer([]string{"admin", "role:addRole"}),
		services.AddRole)

	router.Post("/add/permission",
		middleware.IsRequired(),
		app.FiPer.CheckFiPer([]string{"admin", "role:addPermission"}),
		services.AddPermission)

	router.Post("/add/rolePermission",
		middleware.IsRequired(),
		app.FiPer.CheckFiPer([]string{"admin", "role:addRolePermission"}),
		services.AddRolePermission)

	router.Delete("/delete/role/:name",
		middleware.IsRequired(),
		app.FiPer.CheckFiPer([]string{"admin", "role:deleteRole"}),
		services.DeleteRole)

	router.Delete("/delete/permission/:model/:action",
		middleware.IsRequired(),
		app.FiPer.CheckFiPer([]string{"admin", "role:deletePermission"}),
		services.DeletePermission)

	router.Delete("/delete/rolePermission/:name/:model/:action",
		middleware.IsRequired(),
		app.FiPer.CheckFiPer([]string{"admin", "role:deleteRolePermission"}),
		services.DeleteRolePermission)

	return router
}
