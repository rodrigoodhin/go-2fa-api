package routes

import (
	"github.com/gofiber/fiber/v2"
	"gitlab.com/rodrigoodhin/go-2fa-api/app/services"
	"gitlab.com/rodrigoodhin/go-2fa-api/app/utils/middleware"
)

func AuthRoutes(router fiber.Router) fiber.Router {

	router.Post("/",
		services.Login)

	router.Get("/",
		middleware.IsRequired(),
		services.Profile)

	router.Put("/",
		middleware.IsRequired(),
		services.Refresh)

	router.Delete("/",
		middleware.IsRequired(),
		services.Logout)

	router.Get("/otp",
		middleware.IsRequired(),
		services.OTPGet)

	router.Put("/otp/:passcode",
		middleware.IsRequiredOTP(),
		services.OTPActivate)

	router.Post("/otp/:passcode",
		middleware.IsRequiredOTP(),
		services.OTPLogin)

	return router
}
