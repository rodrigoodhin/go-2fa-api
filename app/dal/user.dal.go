package dal

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"strings"

	"github.com/google/uuid"
	"github.com/raja/argon2pw"
	"github.com/spf13/viper"
	"gitlab.com/rodrigoodhin/fiper/pkgs/fgorm"
	"gitlab.com/rodrigoodhin/go-2fa-api/app"
	"gitlab.com/rodrigoodhin/go-2fa-api/app/types"
	"gitlab.com/rodrigoodhin/go-2fa-api/app/utils"
	"gitlab.com/rodrigoodhin/go-2fa-api/internal/config"
	"gitlab.com/rodrigoodhin/go-2fa-api/internal/database"
	"gitlab.com/rodrigoodhin/go-2fa-api/internal/helpers"
	"gorm.io/datatypes"
	"gorm.io/gorm/clause"
)

func MigrateUser() (err error) {

	if err = database.DB.AutoMigrate(&types.User{}); err != nil {
		return
	}

	hashedPassword, err := argon2pw.GenerateSaltedHash(viper.GetString("defaultAdminUser.password"))
	if err != nil {
		return
	}

	app.FiPer.AddRole(fgorm.FiperRole{
		Name: "admin",
	})

	// Create Default Admin User
	userUpsert := &types.UserUpsert{
		Username: viper.GetString("defaultAdminUser.username"),
		Email:    viper.GetString("defaultAdminUser.email"),
		Password: hashedPassword,
		Status:   true,
		UserType: config.DefaultUserType,
	}

	user, err := UpsertUser(*userUpsert)
	if err != nil {
		return
	}

	_, err = app.FiPer.GrantUserRole(user.ID.String(), "admin")
	if err != nil {
		return
	}

	return
}

func InitUserData() (err error) {
	initData := viper.GetString("initDataPath") + "/users.json"

	if _, err := os.Stat(initData); err == nil {
		users := []types.UserUpsert{}

		content, err := ioutil.ReadFile(initData)
		if err != nil {
			return fmt.Errorf("error reading users data file: %v", err)
		}

		err = json.Unmarshal(content, &users)
		if err != nil {
			return fmt.Errorf("error trying to unmarshal users json file : %v", err)
		}

		for _, u := range users {

			if u.Password, err = helpers.ValidateUserPassOnInitData(types.UserUpsert{Password: u.Password}); err != nil {
				return fmt.Errorf("error to validate User password on init data: %v", err)
			}

			user := types.User{}
			if user, err = UpsertUser(u); err != nil {
				return fmt.Errorf("error adding user on init: %v", err)
			}

			permits := []string{}
			err = json.Unmarshal(u.Permits, &permits)
			if err != nil {
				return fmt.Errorf("error unmarshaling permits of user on init: %v", err)
			}

			for _, permit := range permits {
				p := strings.Split(permit, ":")

				if len(p) == 1 {
					_, err = app.FiPer.GrantUserRole(user.ID.String(), p[0])
					if err != nil {
						return fmt.Errorf("error adding role to user on init: %v", err)
					}

				} else if len(p) == 2 {
					_, err = app.FiPer.GrantUserPermission(u.ID.String(), p[0], p[1])
					if err != nil {
						return fmt.Errorf("error adding permission to user on init: %v", err)
					}
				}

			}
		}
	}

	return
}

func PKUser(userId string) (user types.User, err error) {
	user.ID, err = uuid.Parse(userId)
	if err != nil {
		return
	}

	err = database.DB.First(&user, "id = ?", user.ID).Error

	return
}

func ListUsers() (usersResponse []types.UserResponse, err error) {
	users := []types.User{}
	err = database.DB.Select("id", "username", "email", "status", "user_type", "permits").
		Find(&users).Error
	if err != nil {
		return
	}

	usersResponse = utils.CreateResponseUsers(users)

	return
}

func UpsertUser(userUpsert types.UserUpsert) (user types.User, err error) {
	res := database.DB.Where("id = ? or username = ? or email = ?", userUpsert.ID, userUpsert.Username, userUpsert.Email).First(&user)

	if res.RowsAffected == 0 && userUpsert.Password == "" {
		return user, fmt.Errorf("password cannot be empty")

	} else if res.RowsAffected == 0 { // Insert
		user.Username = userUpsert.Username
		user.Password = userUpsert.Password
		user.Email = userUpsert.Email
		user.Status = userUpsert.Status
		user.UserType = userUpsert.UserType
		user.Permits = datatypes.JSON(`[]`)
		user.OTPRecoveryCodes = datatypes.JSON(`[]`)

		err = database.DB.Select("username", "password", "email", "status", "user_type", "permits", "otp_recovery_codes").
			Create(&user).Error
		if err != nil {
			return
		}

	} else if res.RowsAffected > 0 { // Update
		err = database.DB.Model(&user).
			Select("username", "email", "status", "user_type").
			Updates(types.User{
				Username: userUpsert.Username,
				Email:    userUpsert.Email,
				UserType: userUpsert.UserType,
				Status:   userUpsert.Status,
			}).Error
		if err != nil {
			return
		}
	}

	return
}

func DeleteUser(userId string) (err error) {
	var user types.User

	user.ID, err = uuid.Parse(userId)
	if err != nil {
		return
	}

	res := database.DB.Where("id = ?", userId).
		Delete(&types.User{})
	if res.Error != nil {
		return
	} else if res.RowsAffected == 0 {
		return fmt.Errorf("record not found")
	}

	return
}

func PassResetRequest(email string) (user types.User, err error) {
	res := database.DB.Model(&user).
		Clauses(clause.Returning{}).
		Select("password", "otp_status", "otp_key", "otp_recovery_codes").
		Where("email = ?", email).
		Updates(types.User{
			Password:         *new(string),
			OTPStatus:        false,
			OTPKey:           *new(string),
			OTPRecoveryCodes: datatypes.JSON(`[]`),
		})
	if res.Error != nil {
		return
	}

	if res.RowsAffected == 0 || res.Error != nil {
		return user, fmt.Errorf("record not found")
	}

	return
}

func PassResetConfirm(userId, password string) (err error) {
	res := database.DB.Model(&types.User{}).
		Clauses(clause.Returning{}).
		Select("password").
		Where("id = ? and (password is null or password = '')", userId).
		Updates(types.User{
			Password: password,
		})
	if res.Error != nil {
		return
	}

	if res.RowsAffected == 0 || res.Error != nil {
		return fmt.Errorf("record not found")
	}

	return
}

func PassChange(oldPass, newPass, userId string) (err error) {
	var user types.User

	user.ID, err = uuid.Parse(userId)
	if err != nil {
		return
	}

	err = database.DB.Select("id, password").First(&user, "id = ?", userId).Error
	if user.Password == "" || err != nil {
		return fmt.Errorf("record not found")
	}

	valid, err := argon2pw.CompareHashWithPassword(user.Password, oldPass)
	if !valid || err != nil {
		return fmt.Errorf("invalid current password")
	}

	newPass, err = argon2pw.GenerateSaltedHash(newPass)
	if err != nil {
		return fmt.Errorf("hash generated for new password returned error")
	}

	res := database.DB.Model(&types.User{}).
		Clauses(clause.Returning{}).
		Select("password").
		Where("id = ? and password is not null and password <> ''", userId).
		Updates(types.User{
			Password: newPass,
		})
	if res.Error != nil {
		return
	}

	if res.RowsAffected == 0 || res.Error != nil {
		return fmt.Errorf("password not updated")
	}

	return
}
