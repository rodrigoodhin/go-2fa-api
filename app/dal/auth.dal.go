package dal

import (
	"github.com/raja/argon2pw"
	"gitlab.com/rodrigoodhin/go-2fa-api/app/types"
	"gitlab.com/rodrigoodhin/go-2fa-api/internal/database"
)

func Login(username, password string) (user types.User, err error) {
	err = database.DB.
		Select("id", "permits", "password", "otp_status", "user_type").
		Where("username = ? and password is not null", username).
		First(&user).Error
	if err != nil {
		return
	}

	valid, err := argon2pw.CompareHashWithPassword(user.Password, password)
	if !valid || err != nil {
		return
	}

	return
}

func SetNewOTPKey(user types.User, otpKey string) (err error) {
	err = database.DB.Model(&user).Updates(types.User{OTPKey: otpKey, OTPStatus: false}).Error
	if err != nil {
		return
	}

	return
}

func ActivateOTPKey(user types.User) (err error) {
	err = database.DB.Model(&user).Updates(types.User{OTPStatus: true}).Error
	if err != nil {
		return
	}

	return
}
