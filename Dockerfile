FROM golang:alpine as builder

ARG ENV

RUN apk update && apk add git && apk add --no-cache libc6-compat

RUN apk add -U --no-cache ca-certificates

COPY . $GOPATH/src/gitlab.com/rodrigoodhin/go-2fa-api
WORKDIR $GOPATH/src/gitlab.com/rodrigoodhin/go-2fa-api

RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -a -installsuffix cgo -o /bin/app .

COPY config/$ENV.toml /bin/config/$ENV.toml

# The final image.
FROM scratch

# Copy certificates
COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/

# optional configuraiton files
COPY --from=builder /bin/config/ /config/

# binaries
COPY --from=builder /bin/app /bin/app

EXPOSE 7000

ENTRYPOINT ["/bin/app"]
