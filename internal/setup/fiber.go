package setup

import (
	"fmt"
	"log"
	"os"
	"time"

	sentryfiber "github.com/aldy505/sentry-fiber"
	"github.com/joho/godotenv"
	_ "github.com/joho/godotenv/autoload"
	redoc "github.com/natebwangsut/fiber-redoc"
	"gitlab.com/rodrigoodhin/go-2fa-api/app/dal"
	"gitlab.com/rodrigoodhin/go-2fa-api/app/routes"
	"gitlab.com/rodrigoodhin/go-2fa-api/internal/handlers"

	"gitlab.com/rodrigoodhin/fiper"
	"gitlab.com/rodrigoodhin/fiper/pkgs/fp"
	"gitlab.com/rodrigoodhin/go-2fa-api/app"

	//"github.com/ansrivas/fiberprometheus/v2"
	swagger "github.com/arsmn/fiber-swagger/v2"
	"github.com/getsentry/sentry-go"
	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/cors"
	"github.com/gofiber/fiber/v2/middleware/favicon"
	"github.com/gofiber/fiber/v2/middleware/limiter"
	"github.com/gofiber/fiber/v2/middleware/logger"
	"github.com/gofiber/fiber/v2/middleware/monitor"
	"github.com/gofiber/fiber/v2/middleware/requestid"
	"github.com/gofiber/storage/redis"
	"github.com/spf13/viper"
	_ "gitlab.com/rodrigoodhin/go-2fa-api/docs"
	"gitlab.com/rodrigoodhin/go-2fa-api/internal/database"
	"gitlab.com/rodrigoodhin/go-2fa-api/internal/tokenCache"
)

var (
	err error
)

// Fiber - setup a fiber service with all of its routes
func Fiber() {
	// Load Config
	loadConfig()

	// Start Database
	database.Connect()

	// Init Token Cache
	tokenCache.Init()

	// Init Fiber
	app.Fiber = fiber.New()

	// Set Favicon
	setFavicon()

	// Set Limiter
	setLimiter()

	// Set Sentry
	setSentry()

	// Set RequestID
	setRequestID()

	// Set Cors
	setCors()

	// Set Headers
	setHeaders()

	// Set Monitor
	app.Fiber.Get(viper.GetString("monitoring.monitorPath"), monitor.New())

	// Add Prometheus
	//prometheus := fiberprometheus.New("go2faapi")
	//prometheus.RegisterAt(service, "/metrics")
	//app.Fiber.Use(prometheus.Middleware)

	// Set Docs
	setDocs()

	// Set Logger
	setLogger()

	// Set FiPer
	setFiper()

	// Migrate Database Models
	migrateDatabaseModels()

	// Init Models Data
	initModelsData()

	// Create Routes
	createRoutes()
}

// Set Favicon config
func setFavicon() {
	app.Fiber.Use(favicon.New(favicon.Config{
		File: "./internal/assets/favicon.ico",
	}))
}

// Set RequestID config
func setRequestID() {
	app.Fiber.Use(requestid.New())
}

// Set logger config
func setLogger() {
	file, err := os.OpenFile(viper.GetString("logger.logFile"), os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	if err != nil {
		log.Fatalf("error opening file: %v", err)
	}

	/****************************************************/
	/* Set Logger Level                                 */
	/****************************************************/
	if viper.GetBool("logger.saveLog") {
		app.Fiber.Use(logger.New(logger.Config{
			Format:   "[${time}] - [${ip}]:${port} - ${status} - ${latency} - ${method} - ${path}\n",
			TimeZone: "Europe/Lisbon",
			Output:   file,
		}))
	}
}

// Set Cors config
func setCors() {
	app.Fiber.Use(cors.New(cors.Config{
		AllowOrigins:     "*",
		AllowHeaders:     "Origin, Content-Type, Accept, X-Requested-With, Authorization, sentry-trace",
		AllowCredentials: true,
	}))
}

// Set Headers config
func setHeaders() {
	app.Fiber.Use(func(c *fiber.Ctx) error {
		// Set some security headers:
		c.Set("X-XSS-Protection", "1; mode=block")
		c.Set("X-Content-Type-Options", "nosniff")
		c.Set("X-Download-Options", "noopen")
		c.Set("Strict-Transport-Security", "max-age=5184000")
		c.Set("X-Frame-Options", "SAMEORIGIN")
		c.Set("X-DNS-Prefetch-Control", "off")
		// Go to next middleware:
		return c.Next()
	})
}

// Set limiter config
func setLimiter() {
	app.Fiber.Use(
		limiter.New(limiter.Config{
			Next: func(c *fiber.Ctx) bool {
				return c.IP() == "127.0.0.1"
			},
			Max:        10,
			Expiration: 1 * time.Minute,
			Storage: redis.New(redis.Config{
				Host:     "localhost",
				Port:     18703,
				Database: 0,
				Username: "",
				Password: "go2faapi&33#pass",
				Reset:    false,
			}),
			KeyGenerator: func(c *fiber.Ctx) string {
				return c.IP()
			},
			LimitReached: func(c *fiber.Ctx) error {
				return c.SendStatus(fiber.StatusTooManyRequests)
			},
			SkipSuccessfulRequests: false,
			SkipFailedRequests:     false,
		}),
	)
}

// Set documentation config
func setDocs() {

	// Set swagger endpoint
	app.Fiber.Get("/swagger/*", swagger.New(swagger.Config{ // custom
		URL:          "/swagger/doc.json",
		DeepLinking:  false,
		DocExpansion: "none",
	}))

	// Set redoc endpoint
	app.Fiber.Get("/redoc/*", redoc.Handler)
}

// Set sentry config
func setSentry() {
	var output *os.File
	sentryDebug := false

	if viper.GetBool("sentry.saveLog") {
		sentryDebug = true

		output, err = os.OpenFile(viper.GetString("sentry.logFile"), os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
		if err != nil {
			log.Fatalf("error opening file: %v", err)
		}
	}

	err = sentry.Init(sentry.ClientOptions{
		Dsn:              viper.GetString("monitoring.sentry"),
		Environment:      os.Getenv("ENV"),
		Release:          viper.GetString("release"),
		Debug:            sentryDebug,
		TracesSampleRate: viper.GetFloat64("monitoring.sentryTraceSampleRate"),
		DebugWriter:      output,
	})
	if err != nil {
		log.Fatalln("sentry initialization failed")
	}
	sentry.Flush(time.Second * 2)

	// Add Sentry
	app.Fiber.Use(sentryfiber.New(sentryfiber.Options{
		Repanic: true,
	}))
	app.Fiber.Use(handlers.SentryPerformance())
}

// Set fiper config
func setFiper() {
	app.FiPer = fiper.Gorm(database.DB, &fp.Options{
		UserTableName:    "users",
		UserPermitsField: "permits",
		UserIdField:      "id",
		JwtLocals:        "user",
	})
}

// Load config file and set viper
func loadConfig() {
	// Load .env file
	err = godotenv.Overload(".env")
	if err != nil {
		panic(fmt.Errorf("error loading .env file: %w", err))
	}

	// Load config file
	var env string
	if os.Getenv("GO2FAAPI_ENV") != "" {
		env = os.Getenv("GO2FAAPI_ENV")
	} else {
		env = "local"
	}
	viper.SetConfigName(env)
	viper.SetConfigType("toml")
	viper.AddConfigPath("config/")
	err = viper.ReadInConfig()
	if err != nil {
		panic(fmt.Errorf("fatal error config file: %w", err))
	}
}

func migrateDatabaseModels() {
	//Migrate database user model
	if err = dal.MigrateUser(); err != nil {
		return
	}
}

func initModelsData() {
	// Init user model data
	if err = dal.InitUserData(); err != nil {
		return
	}
}

func createRoutes() {
	// Create API route group
	api := app.Fiber.Group("api")

	// Set Routes
	routes.UserRoutes(api.Group("user"))
	routes.AuthRoutes(api.Group("auth"))
	routes.PermitRoutes(api.Group("permit"))

	// Set index route
	app.Fiber.Get("/", func(c *fiber.Ctx) error {
		return c.SendString(viper.GetString("app") + " 👋!")
	})

	// Handle custom 404 responses
	app.Fiber.Use(func(c *fiber.Ctx) error {
		return c.Status(fiber.StatusNotFound).SendString("Cannot " + c.Method() + " " + c.Path())
	})
}
