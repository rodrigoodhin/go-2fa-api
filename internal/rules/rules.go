package rules

import (
	"regexp"

	validation "github.com/go-ozzo/ozzo-validation/v4"
	"github.com/go-ozzo/ozzo-validation/v4/is"
)

// Patterns
const (
	pName         string = "^[a-zA-Z\\s*]+$"
	pFullName     string = "^[a-zA-Z0-9\\s*]+$"
	pStreet       string = "^[a-zA-Z0-9\\s*]+$"
	pComplement   string = "^[a-zA-Z0-9º\\s*]+$"
	pNumber       string = "^[0-9]+$"
	pZipCode      string = "(^[0-9]{7}$)|(^[0-9]{4}-[0-9]{3}$)"
	pCountry      string = "(^[A-Z]{2}$)"
	pCategoryName string = "^[a-zA-Z'\\-\\s*]+$"
	pSlug         string = "^[a-z0-9\\-]+$"
	pcountryCode  string = "^[A-Z]{2}$"
	pCountryCode  string = "^[A-Z]+$"
)

var (
	UsernameRule = []validation.Rule{
		validation.Required,
		validation.Length(5, 30),
		is.Alphanumeric,
		validation.Required.Error("Must be a valid Username"),
	}

	NameRule = []validation.Rule{
		validation.Required,
		validation.Length(3, 30),
		validation.Match(regexp.MustCompile(pName)),
		validation.Required.Error("Must be a valid Name"),
	}

	SurnameRule = []validation.Rule{
		validation.Length(3, 30),
		validation.Match(regexp.MustCompile(pName)),
		validation.Required.Error("Must be a valid Surname"),
	}

	PhoneNumberRule = []validation.Rule{
		is.E164,
		validation.Required.Error("Must be a valid Phone number"),
	}

	MobileNumberRule = []validation.Rule{
		is.E164,
		validation.Required.Error("Must be a valid Mobline number"),
	}

	PasswordRule = []validation.Rule{
		validation.Required,
		validation.Length(8, 20),
		//validation.Match(regexp.MustCompile("^[0-9]{5}$")), //@TODO Add password characters validation
		validation.Required.Error("Must be a valid Password"),
	}

	EmailRule = []validation.Rule{
		validation.Required,
		is.Email,
		validation.Length(5, 100),
		validation.Required.Error("Must be a valid Email"),
	}

	StatusRule = []validation.Rule{
		validation.Required,
		validation.Required.Error("Must be a valid Status"),
	}

	AddressStreetRule = []validation.Rule{
		validation.Required,
		validation.Match(regexp.MustCompile(pStreet)),
		validation.Length(3, 60),
		validation.Required.Error("Must be a valid address Street"),
	}

	AddressNumberRule = []validation.Rule{
		validation.Required,
		validation.Match(regexp.MustCompile(pNumber)),
		validation.Required.Error("Must be a valid address Number"),
	}

	AddressCityRule = []validation.Rule{
		validation.Required,
		validation.Match(regexp.MustCompile(pName)),
		validation.Length(3, 50),
		validation.Required.Error("Must be a valid address City"),
	}

	AddressComplementRule = []validation.Rule{
		validation.Match(regexp.MustCompile(pComplement)),
		validation.Length(0, 30),
		validation.Required.Error("Must be a valid address Complement"),
	}

	AddressZipCodeRule = []validation.Rule{
		validation.Required,
		validation.Match(regexp.MustCompile(pZipCode)),
		validation.Required.Error("Must be a valid address ZipCode"),
	}

	AddressCountryRule = []validation.Rule{
		validation.Required,
		validation.Match(regexp.MustCompile(pCountry)),
		validation.Required.Error("Must be a valid address Country"),
	}

	FiscalNumberRule = []validation.Rule{
		validation.Required,
		validation.By(checkFiscalNumber),
	}

	CategoryNameRule = []validation.Rule{
		validation.Required,
		validation.Length(3, 30),
		validation.Match(regexp.MustCompile(pCategoryName)),
		validation.Required.Error("Must be a valid Category Name"),
	}

	SlugRule = []validation.Rule{
		validation.Required,
		validation.Length(3, 300),
		validation.Match(regexp.MustCompile(pSlug)),
		validation.Required.Error("Must be a valid Slug"),
	}

	UserTypeRule = []validation.Rule{
		validation.Required,
		validation.By(checkUserType),
	}

	CountryNameRule = []validation.Rule{
		validation.Required,
		validation.Length(3, 50),
		validation.Match(regexp.MustCompile(pName)),
		validation.Required.Error("Must be a valid country Name"),
	}

	CountryCodeRule = []validation.Rule{
		validation.Required,
		validation.Length(2, 2),
		validation.Match(regexp.MustCompile(pCountryCode)),
		validation.Required.Error("Must be a valid country Code"),
	}

	CurrencyNameRule = []validation.Rule{
		validation.Required,
		validation.Length(3, 20),
		validation.Match(regexp.MustCompile(pName)),
		validation.Required.Error("Must be a valid currency Name"),
	}

	CurrencySymbolRule = []validation.Rule{
		validation.Required,
		validation.Length(1, 10),
		validation.Required.Error("Must be a valid currency Symbol"),
	}

	BrandNameRule = []validation.Rule{
		validation.Required,
		validation.Length(3, 30),
		validation.Match(regexp.MustCompile(pFullName)),
		validation.Required.Error("Must be a valid brand Name"),
	}
)
