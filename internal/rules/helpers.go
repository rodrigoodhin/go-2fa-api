package rules

import (
	"errors"

	"gitlab.com/rodrigoodhin/go-2fa-api/internal/config"
	"gitlab.com/rodrigoodhin/go-docs-pt/pkg/nif"
)

func checkFiscalNumber(value interface{}) error {
	s, _ := value.(string)
	if !nif.Validate(s) {
		return errors.New("invalid NIF")
	}
	return nil
}

func checkUserType(value interface{}) error {
	s, _ := value.(string)
	if !stringInSlice(s, config.UserTypes) {
		return errors.New("invalid User type")
	}
	return nil
}

// stringInSlice - Validate if slice contains atring
func stringInSlice(a string, list []string) bool {
	for _, b := range list {
		if b == a {
			return true
		}
	}
	return false
}
