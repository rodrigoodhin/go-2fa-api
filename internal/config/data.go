package config

import (
	"context"

	"github.com/go-redis/redis"
	"github.com/gofiber/fiber/v2"
	"github.com/uptrace/bun"
	"gitlab.com/rodrigoodhin/fiper/pkgs/fgorm"
)

type Data struct {
	DB         *bun.DB
	Api        fiber.Router
	Static     fiber.Router
	TokenCache *redis.Client
	FP         *fgorm.FiPer
	Ctx        context.Context
	Service    *fiber.App
}
