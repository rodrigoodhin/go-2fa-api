package config

var (
	UserTypes = []string{"employee", "customer"}
)

const (
	DefaultUserType = "employee"
	DefaultCustomerType = "customer"
	DefaultOffset = 0
	DefaultLimit = 10
	DefaultSortField = "created_at"
	DefaultSortOrder = "desc"
)