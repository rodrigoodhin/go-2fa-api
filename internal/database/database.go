package database

import (
	"context"
	"log"
	"os"
	"time"

	"github.com/spf13/viper"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
)

var (
	err error
	DB  *gorm.DB
	Ctx = context.Background()
)

func Connect() {
	dsn := "host=" + viper.GetString("database.host") +
		" user=" + viper.GetString("database.user") +
		" password=" + viper.GetString("database.password") +
		" dbname=" + viper.GetString("database.dbName") +
		" port=" + viper.GetString("database.port") +
		" sslmode=disable TimeZone=Europe/Lisbon"
	DB, err = gorm.Open(postgres.Open(dsn), &gorm.Config{
		Logger: setLog(),
	  })

	if err != nil {
		log.Panicf("failed to connect database: %s", err.Error())
	}
}

// setLog - Configure db log
func setLog() logger.Interface {
	file, err := os.OpenFile(viper.GetString("database.logFile"), os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	if err != nil {
		log.Fatalf("error opening file: %v", err)
	}

	cfg := logger.Config{}

	switch viper.GetString("database.logLevel") {
	case "Silent":
		cfg = logger.Config{
			SlowThreshold:             time.Second,
			LogLevel:                  logger.Silent,
			IgnoreRecordNotFoundError: true,
			Colorful:                  false,
		}
	case "Warn":
		cfg = logger.Config{
			SlowThreshold:             time.Second,
			LogLevel:                  logger.Warn,
			IgnoreRecordNotFoundError: false,
			Colorful:                  true,
		}
	case "Error":
		cfg = logger.Config{
			SlowThreshold:             time.Second,
			LogLevel:                  logger.Error,
			IgnoreRecordNotFoundError: false,
			Colorful:                  true,
		}
	case "Debug", "Info", "Fatal", "Panic", "Trace":
		cfg = logger.Config{
			SlowThreshold:             time.Second,
			LogLevel:                  logger.Info,
			IgnoreRecordNotFoundError: false,
			Colorful:                  true,
		}
	}

	return logger.New(
		log.New(file, "\r\n", log.LstdFlags),
		cfg,
	)
}
