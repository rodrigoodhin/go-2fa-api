package database

import (
	"strconv"

	"gitlab.com/rodrigoodhin/go-2fa-api/internal/config"
)

type List struct {
	Offset    string
	Limit     string
	SortField string
	SortOrder string
}

func (l List) GetOffset() (r int) {
	r, err := strconv.Atoi(l.Offset)
	if err != nil {
		return config.DefaultOffset
	}

	return
}

func (l List) GetLimit() (r int) {
	r, err := strconv.Atoi(l.Limit)
	if err != nil {
		return config.DefaultLimit
	}

	return
}
