package database

import (
	"strconv"
	"strings"

	"gitlab.com/rodrigoodhin/go-2fa-api/internal/config"
)

type Search struct {
	Terms       []Term  `json:"terms"`
	TermsConcat string  `json:"terms_concat"`
	Offset      string  `json:"offset"`
	Limit       string  `json:"limit"`
	Order       []Order `json:"order"`
}

type Term struct {
	Field string `json:"field"`
	Value string `json:"value"`
}

type Order struct {
	Field string `json:"field"`
	Type  string `json:"type"`
}

func (s Search) GetOffset() int {
	o, err := strconv.Atoi(s.Offset)
	if err != nil {
		return config.DefaultOffset
	}

	return o
}

func (s Search) GetLimit() int {
	l, err := strconv.Atoi(s.Limit)
	if err != nil {
		return config.DefaultLimit
	}

	return l
}

func (s Search) GetTerms() (terms string) {
	if len(s.Terms) == 0 {
		terms = "1=1"

	} else if strings.ToLower(s.TermsConcat) != "and" && strings.ToLower(s.TermsConcat) != "or" {
		terms = "1=1"

	} else {
		for _, t := range s.Terms {
			terms += t.Field + " LIKE '" + t.Value + "' " + s.TermsConcat + " "
		}

		i := strings.LastIndex(terms, " "+s.TermsConcat+" ")
		terms = terms[:i]
	}
	return
}

func (s Search) GetOrder() (order string) {
	if len(s.Order) == 0 {
		order = "id DESC"

	} else {
		for _, v := range s.Order {
			if v.Field == "" {
				v.Field = "id"

			} else if v.Type == "" || (v.Type != "asc" && v.Type != "desc") {
				v.Type = "desc"
			}

			order += v.Field + " " + v.Type + ","
		}

		i := strings.LastIndex(order, ",")
		order = order[:i]
	}

	return
}
