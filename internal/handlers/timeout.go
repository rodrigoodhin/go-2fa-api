package handlers

import (
	"time"

	"github.com/gofiber/fiber/v2"
)

const Timeout = 5 * time.Second

func TimeoutHandler(ctx *fiber.Ctx) error {
	err := ctx.SendString("Timeout error")
	if err != nil {
		return err
	}
	return nil
}