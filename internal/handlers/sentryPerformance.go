package handlers

import (
	"github.com/getsentry/sentry-go"
	"github.com/gofiber/fiber/v2"
	"github.com/spf13/viper"
)

// SentryPerformance - Implements fiber MiddlewareFunc.
func SentryPerformance() fiber.Handler {
	return func(c *fiber.Ctx) (err error) {
		if c.Path() != viper.GetString("monitoring.monitorPath") {
			Span := sentry.StartSpan(c.Context(), c.Path(),
				sentry.TransactionName(c.Hostname()+c.Path()))

			defer Span.Finish()
		}

		c.Next()

		return
	}
}
