package tokenCache

import (
	"github.com/go-redis/redis"
	"github.com/spf13/viper"
	"gitlab.com/rodrigoodhin/go-2fa-api/app"
)

// Init - Init data store instance to save users tokens info
func Init() {
	app.TokenCache = redis.NewClient(&redis.Options{
		Addr:     viper.GetString("sessionCache.url"),
		Password: viper.GetString("sessionCache.password"),
		DB:       0,
	})
}
