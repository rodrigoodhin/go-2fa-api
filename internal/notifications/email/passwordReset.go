package email

import (
	"bytes"
	"embed"
	"fmt"
	"html/template"

	"gitlab.com/rodrigoodhin/go-2fa-api/app/types"
)

//go:embed htmlTmpl/*
var htmlTmpl embed.FS

func SendPasswordResetEmail(info *types.PassResetHTML) (err error) {
	tmpl, err := template.ParseFS(htmlTmpl, "htmlTmpl/password-reset.html")
	if err != nil {
		return fmt.Errorf("error trying to load html embedded : %v", err)
	}

	buf := new(bytes.Buffer)
	if err = tmpl.Execute(buf, &info); err != nil {
		return fmt.Errorf("error trying to parse chart file : %v", err)
	}

	return sendEmail("Password Reset", buf.String(), []string{info.Email})
}
