package email

import (
	"encoding/base64"
	"net/smtp"
	"strings"

	"github.com/spf13/viper"
)

func sendEmail(subject, body string, to []string) error {
	r := strings.NewReplacer("\r\n", "", "\r", "", "\n", "", "%0a", "", "%0d", "")

	c, err := smtp.Dial(viper.GetString("email.smtpHost"))
	if err != nil {
		return err
	}
	defer c.Close()

	if err = c.Mail(r.Replace(viper.GetString("email.noReplyEmail"))); err != nil {
		return err
	}

	for i := range to {
		to[i] = r.Replace(to[i])
		if err = c.Rcpt(to[i]); err != nil {
			return err
		}
	}

	w, err := c.Data()
	if err != nil {
		return err
	}

	msg := "To: " + strings.Join(to, ",") + "\r\n" +
		"From: " + viper.GetString("email.noReplyEmail") + "\r\n" +
		"Subject: " + subject + "\r\n" +
		"Content-Type: text/html; charset=\"UTF-8\"\r\n" +
		"Content-Transfer-Encoding: base64\r\n" +
		"\r\n" + base64.StdEncoding.EncodeToString([]byte(body))

	_, err = w.Write([]byte(msg))
	if err != nil {
		return err
	}

	err = w.Close()
	if err != nil {
		return err
	}

	return c.Quit()
}
