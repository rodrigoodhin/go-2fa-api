package helpers

import (
	"fmt"
	"log"
	"strconv"
	"strings"

	"github.com/gofiber/fiber/v2"
	"github.com/hesahesa/pwdbro"
	"github.com/raja/argon2pw"
	"gitlab.com/rodrigoodhin/go-2fa-api/app/types"
	"gitlab.com/rodrigoodhin/go-2fa-api/internal/config"
	"gitlab.com/rodrigoodhin/go-2fa-api/internal/database"
)

func Contains(elems []string, v string) bool {
	for _, s := range elems {
		if v == s {
			return true
		}
	}
	return false
}

// CheckPassStrength - Check user password strength
func CheckPassStrength(pwd string) (isValid bool, msgs []string, errors []string) {
	status, err := pwdbro.NewDefaultPwdBro().RunParallelChecks(pwd)
	if err != nil {
		log.Fatalf("Failed to validate password: %v", err)
	}

	isValid = true
	for _, resp := range status {
		if !resp.Safe && isValid {
			isValid = resp.Safe
		}

		msgs = append(msgs, resp.Message)

		if resp.Error != nil {
			errors = append(errors, resp.Error.Error())
		}
	}

	return
}

// ValidateUserPassOnInitData - Validate and generate hash from user password on init data
func ValidateUserPassOnInitData(user types.UserUpsert) (pass string, err error) {
	if err = user.ValidatePassword(); err != nil {
		return pass, fmt.Errorf("error to validate User password: %v", err)
	}

	if isValid, msgs, errors := CheckPassStrength(user.Password); !isValid {
		return pass, fmt.Errorf("error checking password strength: %v\nMsgs: %v\nErros: %v", err, strings.Join(msgs, ", "), strings.Join(errors, ", "))
	}

	if pass, err = argon2pw.GenerateSaltedHash(user.Password); err != nil {
		return pass, fmt.Errorf("hash generated returned error: %v", err)
	}

	return
}

func SearchParams(c *fiber.Ctx) database.List {
	return database.List{
		Offset:    c.Query("offset", strconv.Itoa(config.DefaultOffset)),
		Limit:     c.Query("limit", strconv.Itoa(config.DefaultLimit)),
		SortField: c.Query("sortField", config.DefaultSortField),
		SortOrder: c.Query("sortOrder", config.DefaultSortOrder),
	}
}
