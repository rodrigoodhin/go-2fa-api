package main

import (
	"log"

	"gitlab.com/rodrigoodhin/go-2fa-api/app"
	_ "gitlab.com/rodrigoodhin/go-2fa-api/docs"
	"gitlab.com/rodrigoodhin/go-2fa-api/internal/setup"
)

// @title Go2faApi
// @version 1.0
// @description Swagger for Go2faApi
// @contact.name Go2faApi Support
// @contact.email rodrigoodhin@gmail.com
// @license.name Apache 2.0
// @license.url http://www.apache.org/licenses/LICENSE-2.0.html
// @host localhost:7000
// @securityDefinitions.apikey ApiKeyAuth
// @in header
// @name ApiKeyAuth
// @BasePath /api
func main() {
	// Setup a fiber service
	setup.Fiber()

	// Start Fiber
	if err := app.Fiber.Listen(":7000"); err != nil {
		log.Fatalln(err.Error())
	}
}
