# API

Go2faApi

## Description
Go2faApi is an Golang API with all that you need to control users and roles.

## Initiating the containers
```
docker-compose up -d
```

## Usage
go run .

## Swagger
*[Local](http://localhost:7000/swagger/)*
